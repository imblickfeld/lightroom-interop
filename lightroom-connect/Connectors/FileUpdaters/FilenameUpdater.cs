﻿using imBlickFeld.Lightroom.Connect.Connectors.Extractors;
using imBlickFeld.Lightroom.Entities;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace imBlickFeld.Lightroom.Connect.Connectors.FileUpdaters
{
    internal class FilenameUpdater : BaseFileUpdater
    {
        public FilenameUpdater(string fieldName) : base(fieldName) { }

        public override string HelpText => "updates image file name";

        public override bool Run(LRFile file, IList<IFieldExtractor> extractors, CatalogService service, bool dryRun) {
            string src = file.RootDirectory + file.Directory + file.FileName;
            if (!File.Exists(src)) {
                if (Config.I.SkipMissing)
                    return false;
                else
                    throw new FileNotFoundException("file not found");
            }
            string filename = extractors.Extract(file);
            Config.I.LogDebug($"Renaming {src} --> {filename}...");
            if (!dryRun)
                File.Move(src, file.RootDirectory + file.Directory + filename);
            service.UpdateFileName(file.ID, Path.GetFileNameWithoutExtension(filename), Path.GetExtension(filename).Substring(1));
            return true;
        }
    }
}