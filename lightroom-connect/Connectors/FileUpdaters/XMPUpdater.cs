﻿using imBlickFeld.Lightroom.Connect.Connectors.Extractors;
using imBlickFeld.Lightroom.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace imBlickFeld.Lightroom.Connect.Connectors.FileUpdaters
{
    internal class XMPUpdater : IFileUpdater
    {
        #region Supported FieldNames

        public const string TITLE = "title";
        public const string DESCRIPTION = "description";

        #endregion

        private IDictionary<ulong, IList<LRMetaData>> _metadataForFiles;

        public string FieldName { get; private set; }

        public string HelpText => "updates " + FieldName + " metadata";

        public XMPUpdater(string fieldName) {
            FieldName = fieldName;
        }

        public void Init(CatalogService service) {
            _metadataForFiles = service.QueryMetadataForFiles(Config.I.Conditions);
        }

        public bool Run(LRFile file, IList<IFieldExtractor> extractors, CatalogService service, bool dryRun) {
            string value = extractors.Extract(file);
            if (string.IsNullOrEmpty(value))
                return false;
            IList<LRMetaData> mdList;
            try {
                mdList = _metadataForFiles[file.ID];
            } catch (KeyNotFoundException) {
                throw new InvalidDataException("No metadata found for file with ID " + file.ID);
            }
            foreach (var md in mdList) {
                switch (FieldName) {
                    case TITLE:
                        md.Title = value; break;
                    case DESCRIPTION:
                        md.Description = value; break;
                    default:
                        throw new NotSupportedException("Unsupported XMP field: " + FieldName);
                }
                service.UpdateMetadata(md.ID, md.ToString());
            }
            return true;
        }
    }
}