﻿using imBlickFeld.Lightroom.Connect.Connectors.Extractors;
using imBlickFeld.Lightroom.Entities;
using System.Collections.Generic;
using System.Text;

namespace imBlickFeld.Lightroom.Connect.Connectors.FileUpdaters
{
    abstract class BaseFileUpdater : IFileUpdater
    {
        public string FieldName { get; }

        public abstract string HelpText { get; }

        protected BaseFileUpdater(string fieldName) {
            FieldName = fieldName;
        }

        public virtual void Init(CatalogService service) { }

        public abstract bool Run(LRFile file, IList<IFieldExtractor> extractors, CatalogService service, bool dryRun);
    }

    public static class IFieldExtractorExtensions
    {
        public static string Extract(this IList<IFieldExtractor> extractors, LRFile file) {
            var sb = new StringBuilder();
            foreach (var ex in extractors) {
                sb.Append(ex.Extract(file));
            }
            return sb.ToString();
        }
    }
}
