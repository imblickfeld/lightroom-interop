﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.FileUpdaters
{
    class FileUpdaterFactory
    {
        public static readonly FileUpdaterFactory I = new FileUpdaterFactory();

        private FileUpdaterFactory() { }

        private Dictionary<string, IFileUpdater> _updaters = new Dictionary<string, IFileUpdater> {
            { "filename", new FilenameUpdater("filename") },
            { XMPUpdater.TITLE, new XMPUpdater(XMPUpdater.TITLE) },
            { XMPUpdater.DESCRIPTION, new XMPUpdater(XMPUpdater.DESCRIPTION) }
        };

        public IFileUpdater GetUpdater(string name) {
            if (!_updaters.TryGetValue(name.ToLowerInvariant(), out IFileUpdater result))
                throw new ArgumentException($"Unknown updater: {name}.");
            return result;
        }

        public IEnumerable<KeyValuePair<string, string>> GetUpdaterHelp() {
            return _updaters.Select(x => new KeyValuePair<string, string>(x.Key, x.Value.HelpText));
        }
    }
}
