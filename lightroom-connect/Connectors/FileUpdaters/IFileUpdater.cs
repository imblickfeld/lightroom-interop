﻿using imBlickFeld.Lightroom.Connect.Connectors.Extractors;
using imBlickFeld.Lightroom.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.FileUpdaters
{
    interface IFileUpdater
    {
        string FieldName { get; }

        string HelpText { get; }

        void Init(CatalogService service);

        bool Run(LRFile file, IList<IFieldExtractor> extractors, CatalogService service, bool dryRun);
    }
}
