﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    class SequenceCommand : BaseRegexReplaceCommand
    {
        private int _count;
        private string _formatString;

        public override string Name => "sequence";
        public override int ExpectedParameters => 2;
        public override string HelpText => "sequence to increase with every file";
        public override string Syntax => Name + "(starting value)(number of digits)";

        protected override void ValidateParameters(string[] parameters) {
            _count = int.Parse(parameters[0]) - 1;
            int numberOfDigits = int.Parse(parameters[1]);
            if (numberOfDigits < 1)
                throw new ArgumentException("number of digits must not be lower than 1");
            _formatString = "D" + numberOfDigits;
        }

        public override string Run(string[] inputParams) {
            return (++_count).ToString(_formatString);
        }
    }
}
