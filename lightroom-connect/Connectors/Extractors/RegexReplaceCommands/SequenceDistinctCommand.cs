﻿using System;
using System.Collections.Generic;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    class SequenceDistinctCommand : BaseRegexReplaceCommand
    {
        private readonly Dictionary<string /*restartGroup*/, Dictionary<string /*group*/, int>> _groups = new Dictionary<string, Dictionary<string, int>>();
        private readonly Dictionary<string /*restartGroup*/, int> _counts = new Dictionary<string, int>();
        private int _defaultCount;
        private string _formatString;

        public override string Name => "sequence-distinct";

        public override int ExpectedParameters => 3;
        public override int OptionalParameters => 1;

        public override string HelpText => "sequence to increase with every group";

        public override string Syntax => Name + "(value to group by)(starting value)(number of digits)(optional value to restart sequence)";

        protected override void ValidateParameters(string[] parameters) {
            _defaultCount = int.Parse(parameters[1]) - 1;
            int numberOfDigits = int.Parse(parameters[2]);
            if (numberOfDigits < 1)
                throw new ArgumentException("number of digits must not be lower than 1");
            _formatString = "D" + numberOfDigits;
        }

        public override string Run(string[] inputParams) {
            string group = ReplaceGroups(Parameters[0], inputParams);
            string restartGroup = Parameters.Length > 3 ? ReplaceGroups(Parameters[3], inputParams) : "";
            if (!_groups.TryGetValue(restartGroup, out var gg)) {
                gg = new Dictionary<string, int>();
                _groups.Add(restartGroup, gg);
                _counts.Add(restartGroup, _defaultCount);
            }
            if (!gg.TryGetValue(group, out int value)) {
                value = _counts[restartGroup] + 1;
                _counts[restartGroup] = value;
                gg.Add(group, value);
            }
            return value.ToString(_formatString);
        }
    }
}
