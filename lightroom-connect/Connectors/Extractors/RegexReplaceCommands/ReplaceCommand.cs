﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    class ReplaceCommand : BaseRegexReplaceCommand
    {
        public override string Name => "replace";

        public override int ExpectedParameters => 3;

        public override string HelpText => "in search value replace old value by new value";

        public override string Syntax => Name + "(search value)(old value)(new value)";

        public override string Run(string[] inputParams) {
            string value = ReplaceGroups(Parameters[0], inputParams);
            string o = ReplaceGroups(Parameters[1], inputParams);
            string n = ReplaceGroups(Parameters[2], inputParams);
            return value.Replace(o, n);
        }
    }
}
