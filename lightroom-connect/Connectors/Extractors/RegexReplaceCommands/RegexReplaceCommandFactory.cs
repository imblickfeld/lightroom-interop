﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    class RegexReplaceCommandFactory
    {
        public static readonly RegexReplaceCommandFactory I = new RegexReplaceCommandFactory();
        public const string TEXT_COMMAND = "text";

        private readonly IDictionary<string, CommandInfo> _commands;

        private RegexReplaceCommandFactory() {
            var intf = typeof(IRegexReplaceCommand);
            _commands = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => intf.IsAssignableFrom(x) && !x.IsAbstract && !x.IsInterface)
                .Select(x => new CommandInfo(x.GetConstructor(Type.EmptyTypes)))
                .ToDictionary(x => x.Name, x => x);
        }

        public IRegexReplaceCommand GetCommand(string name, string[] parameters) {
            if (!_commands.TryGetValue(name, out CommandInfo info))
                throw new ArgumentException("unknown command: " + name);
            var cmd = info.CreateCommand();
            if (cmd.ExpectedParameters > parameters.Length || cmd.ExpectedParameters + cmd.OptionalParameters < parameters.Length) {
                string optParams = cmd.OptionalParameters != 0 ? $" and {cmd.OptionalParameters} optional parameter{(cmd.OptionalParameters != 1 ? "s" : "")}" : "";
                throw new ArgumentException($"{name}: {cmd.ExpectedParameters} mandatory parameter{(cmd.ExpectedParameters != 1 ? "s" : "")}{optParams} were expected, but {parameters.Length} were given.");
            }
            try {
                cmd.Parameters = parameters;
            } catch (Exception e) {
                throw new ArgumentException($"{name}: {e.Message}", e);
            }
            return cmd;
        }

        public IEnumerable<Help> HelpText => _commands.Select(x => x.Value.Help);

        public class Help
        {
            public readonly string Key;
            public readonly string HelpText;
            public readonly string Syntax;

            public Help(IRegexReplaceCommand cmd) {
                Key = cmd.Name;
                HelpText = cmd.HelpText;
                Syntax = cmd.Syntax;
            }
        }

        private class CommandInfo
        {
            public readonly string Name;
            public readonly ConstructorInfo Ctor;
            public readonly Help Help;

            public CommandInfo(ConstructorInfo ctor) {
                var cmd = ctor.Invoke(null) as IRegexReplaceCommand;
                Name = cmd.Name;
                Help = new Help(cmd);
                Ctor = ctor;
            }

            public IRegexReplaceCommand CreateCommand() {
                return Ctor.Invoke(null) as IRegexReplaceCommand;
            }
        }
    }
}
