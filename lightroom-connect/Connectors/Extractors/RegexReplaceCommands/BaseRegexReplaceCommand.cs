﻿using System.Diagnostics;
using System.Text.RegularExpressions;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    [DebuggerDisplay("{Name}")]
    abstract class BaseRegexReplaceCommand : IRegexReplaceCommand
    {
        private static readonly Regex GROUP_PATTERN = new Regex(@"\\\d+");

        private string[] _parameters;
        public string[] Parameters {
            get => _parameters;
            set {
                ValidateParameters(value);
                _parameters = value; 
            }
        }

        public abstract string Name { get; }
        public abstract int ExpectedParameters { get; }
        public virtual int OptionalParameters => 0;
        public abstract string HelpText { get; }
        public abstract string Syntax { get; }

        public abstract string Run(string[] inputParams);

        protected virtual void ValidateParameters(string[] parameters) { }

        protected string ReplaceGroups(string value, string[] inputParams) {
            // Replace \X groups with corresponding value:
            for (int i = inputParams.Length - 1; i >= 0; --i) {
                value = value.Replace("\\" + i, inputParams[i]);
            }
            // Replace non-existing groups with "":
            value = GROUP_PATTERN.Replace(value, "");
            return value;
        }
    }
}
