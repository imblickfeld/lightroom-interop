﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    class TextCommand : BaseRegexReplaceCommand
    {
        public override string Name => RegexReplaceCommandFactory.TEXT_COMMAND;
        public override int ExpectedParameters => 1;
        public override string HelpText => "returns the given value";
        public override string Syntax => "text(value)";

        public override string Run(string[] inputParams) {
            return ReplaceGroups(Parameters[0], inputParams);
        }
    }
}
