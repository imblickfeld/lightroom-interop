﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    class ToLowerCommand : BaseRegexReplaceCommand
    {
        public override string Name => "tolower";

        public override int ExpectedParameters => 1;

        public override string HelpText => "converts a string to lowercase";

        public override string Syntax => Name + "(string)";

        public override string Run(string[] inputParams) {
            return ReplaceGroups(Parameters[0], inputParams).ToLower();
        }
    }
}
