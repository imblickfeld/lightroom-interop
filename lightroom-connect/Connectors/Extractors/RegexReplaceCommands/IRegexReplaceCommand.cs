﻿namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    public interface IRegexReplaceCommand
    {
        string Name { get; }
        int ExpectedParameters { get; }
        int OptionalParameters { get; }
        string HelpText { get; }
        string Syntax { get; }
        string[] Parameters { get; set; }

        string Run(string[] inputParams);
    }
}
