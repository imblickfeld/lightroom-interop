﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    public class RegexReplaceCommandList : List<IRegexReplaceCommand>
    {
        public RegexReplaceCommandList() { }

        public RegexReplaceCommandList(int capacity) : base(capacity) { }

        public RegexReplaceCommandList(IEnumerable<IRegexReplaceCommand> collection) : base(collection) { }

        public string Run(string[] regexGroups) {
            StringBuilder sb = new StringBuilder();
            foreach (var cmd in this) {
                try {
                    sb.Append(cmd.Run(regexGroups));
                } catch (Exception e) {
                    throw new InvalidOperationException($"{cmd.Name}: {e.Message}", e);
                }
            }
            return sb.ToString();
        }
    }
}
