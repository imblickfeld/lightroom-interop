﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands
{
    class ReplaceIfCommand : BaseRegexReplaceCommand
    {
        private static readonly string[] EMPTY = new string[0];

        private string[] _oldValues;
        private string[] _newValues;

        public override string Name => "replace-if";
        public override int ExpectedParameters => 3;
        public override string HelpText => "in search value replace old value by corresponding new value if it matches; ! may by any <SEP>";
        public override string Syntax => Name + "(search value)(!old value A!old B!old C)(!new value A!new B!new C)";

        protected override void ValidateParameters(string[] parameters) {
            _oldValues = SplitValues(parameters[1]);
            _newValues = SplitValues(parameters[2]);
            if (_oldValues.Length != _newValues.Length)
                throw new ArgumentException($"number of old values ({_oldValues.Length}) does not match number of new values ({_newValues.Length})");
        }

        public override string Run(string[] inputParams) {
            string value = ReplaceGroups(Parameters[0], inputParams);
            for (int i = 1; i < _oldValues.Length; ++i) {
                var o = ReplaceGroups(_oldValues[i], inputParams);
                var n = ReplaceGroups(_newValues[i], inputParams);
                value = value.Replace(o, n);
            }
            return value;
        }

        private string[] SplitValues(string values) {
            if (values.Length == 0)
                return EMPTY;
            char separator = values[0];
            return values.Split(separator);
        }
    }
}
