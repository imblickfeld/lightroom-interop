﻿using imBlickFeld.Lightroom.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors
{
    public interface IFieldExtractor
    {
        string Extract(LRFile file);
    }
}
