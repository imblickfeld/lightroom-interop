﻿using imBlickFeld.Lightroom.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors
{
    [DebuggerDisplay("Literal: {Value}")]
    public class LiteralExtractor : IFieldExtractor
    {
        public string Value { get; }

        public LiteralExtractor(string value) {
            Value = value;
        }

        public string Extract(LRFile file) {
            return Value;
        }
    }
}
