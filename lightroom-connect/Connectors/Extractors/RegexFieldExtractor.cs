﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands;
using imBlickFeld.Lightroom.Entities;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors
{
    [DebuggerDisplay("Regex: {FieldName}, {SearchRegex}, {ReplaceCommandList}")]
    public class RegexFieldExtractor : IFieldExtractor
    {
        private static readonly Regex BUILDER_PARSE_REGEX = new Regex(@"\\(\d+)|\\(([a-z0-9-_]+)(\(([^\(]*)\))+)");

        public string FieldName { get; private set; }
        public Regex SearchRegex { get; private set; }
        public RegexReplaceCommandList ReplaceCommandList { get; private set; }

        public RegexFieldExtractor(StringBuilder sb, int start, int count) {
            int pos = start;
            char separator = sb[pos];

            // Extract FieldName
            ++pos;
            int i = 0;
            try {
                while (sb[pos + (i++)] != separator) ;
                if (pos + i - 2 > start + count)
                    throw new IndexOutOfRangeException();
            } catch (IndexOutOfRangeException) {
                throw new ArgumentException($"{{{sb.ToString(start, count)}}} - invalid syntax: {separator} only found once."); ;
            }
            FieldName = sb.ToString(pos, i - 1);

            // Extract SearchPattern
            pos += i;
            i = 0;
            try {
                while (sb[pos + (i++)] != separator) ;
                if (pos + i - 2 > start + count)
                    throw new IndexOutOfRangeException();
            } catch (IndexOutOfRangeException) {
                throw new ArgumentException($"{{{sb.ToString(start, count)}}} - invalid syntax: {separator} only found twice."); ;
            }
            SearchRegex = new Regex(sb.ToString(pos, i - 1));

            // Extract ReplaceBuilder string
            ReplaceCommandList = ParseReplaceCommands(sb.ToString(pos + i, start + count - pos - i));
        }

        private RegexReplaceCommandList ParseReplaceCommands(string pattern) {
            var factory = RegexReplaceCommandFactory.I;
            var matches = BUILDER_PARSE_REGEX.Matches(pattern)
                .Cast<Match>()
                .Select(x => {
                    var g = x.Groups;
                    if (g[1].Value == "") // named command
                        return factory.GetCommand(g[3].Value, g[5].Captures.Cast<Capture>().Where(y => y.Value != "").Select(y => y.Value).ToArray());
                    else // simple group replacements such as \1
                        return factory.GetCommand(RegexReplaceCommandFactory.TEXT_COMMAND, new[] { "\\" + g[1].Value });
                });
            return new RegexReplaceCommandList(matches);
        }

        public string Extract(LRFile file) {
            string value;
            switch (FieldName) {
                case "id": value = file.ID.ToString(); break;
                case "root-directory": value = file.RootDirectory; break;
                case "directory": value = file.Directory; break;
                case "filename": value = file.FileName; break;
                case "basename": value = file.BaseName; break;
                case "ext": value = file.Extension; break;
                case "sidecar-filename": value = file.SidecarFileName; break;
                default: throw new ArgumentException("unsupported field: " + FieldName);
            }
            var groups = SearchRegex.Match(value).Groups.Cast<Group>().Select(x => x.Value).ToArray();
            return ReplaceCommandList.Run(groups);
        }

        public static Dictionary<string, string> FieldHelpText => new Dictionary<string, string> {
            { "id", "file database ID (AgLibraryFile.id_local)" },
            { "root-directory", "file root directory" },
            { "directory", "file directory path (without root directory part)" },
            { "filename", "full file name" },
            { "basename", "file name without extension" },
            { "ext", "file extension" },
            { "sidecar-filename", "full name or the file's sidecar file" }
        };
    }
}
