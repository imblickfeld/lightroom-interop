﻿using System;
using System.Collections.Generic;
using System.Text;

namespace imBlickFeld.Lightroom.Connect.Connectors.Extractors
{
    public class FieldExtractorFactory
    {
        public static readonly FieldExtractorFactory I = new FieldExtractorFactory();
        private FieldExtractorFactory() { }

        public IList<IFieldExtractor> FromString(string s) {
            var list = new List<IFieldExtractor>();
            var sb = new StringBuilder(s);
            int pos = 0;
            int count;
            Extractors ex;
            while ((ex = FindNextExtractorString(sb, pos, out count)) != Extractors.None) {
                list.Add(FromStream(ex, sb, pos, count));
                pos += count;
            }
            return list;
        }

        private IFieldExtractor FromStream(Extractors ex, StringBuilder sb, int pos, int count) {
            switch (ex) {
                case Extractors.Literal:
                    return new LiteralExtractor(sb.ToString(pos, count));
                case Extractors.Regex:
                    return new RegexFieldExtractor(sb, pos + 1, count - 2);
                default:
                    throw new ArgumentException("Unsupported extractor: " + ex);
            }
        }

        private enum Extractors
        {
            None,
            Literal,
            Regex
        }

        private Extractors FindNextExtractorString(StringBuilder sb, int start, out int count) {
            int len = sb.Length;
            if (start >= len) {
                count = 0;
                return Extractors.None;
            }
            int pos = start - 1;

            // Literal
            while (pos++ < len - 1 && sb[pos] != '{') ;
            count = pos - start;
            if (count > 0)
                return Extractors.Literal;

            // Regex
            ++pos;
            int bracketStack = 1;
            while (bracketStack > 0) {
                try {
                    while (sb[pos++] != '}') {
                        if (sb[pos - 1] == '{')
                            ++bracketStack;
                    }
                } catch (IndexOutOfRangeException) {
                    throw new ArgumentException($"\"{{{sb.ToString(start, pos - start - 1)}\" misses an ending }}.");
                }
                --bracketStack;
            }
            count = pos - start;
            if (count > 0)
                return Extractors.Regex;
            return Extractors.None;
        }
    }
}
