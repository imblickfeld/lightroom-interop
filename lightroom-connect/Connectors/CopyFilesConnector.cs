﻿using imBlickFeld.Lightroom.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace imBlickFeld.Lightroom.Connect.Connectors
{
    class CopyFilesConnector : BaseConnector
    {
        public string TargetRoot { get; private set; }
        public bool Overwrite { get; private set; }
        public bool SkipExisting { get; private set; }
        public bool Move { get; private set; }
        public bool DryRun { get; private set; }

        protected override RunResult DoRun(CatalogService service) {
            ulong count = 0, copied = 0, skipped = 0, error = 0;

            string targetRoot = TargetRoot;
            bool overwrite = Overwrite;
            bool move = Move;
            bool skipMissing = _cfg.SkipMissing;
            bool dryRun = DryRun;
            string sDryRun = dryRun ? "[Dry Run] " : "";

            if (dryRun)
                _cfg.LogInfo("This is a dry run: I will not copy anything.");
            IList<LRFile> filenames;
            if (string.IsNullOrWhiteSpace(_cfg.Sql))
                filenames = service.QueryFileNames(_cfg.Conditions, null, _cfg.OrderByFields, _cfg.IgnoreCase).ToList();
            else
                filenames = service.QueryFileNames(_cfg.Sql).ToList();
            int max = filenames.Count;
            int i = 0;
            foreach (var f in filenames) {
                _cfg.WriteProgress(i++, max);
                string sourceFile = f.RootDirectory + f.Directory + f.FileName;
                if (!File.Exists(sourceFile)) {
                    if (skipMissing) {
                        _cfg.LogDebug($"Skipping missing source file {sourceFile}.");
                        ++skipped;
                    } else {
                        _cfg.LogError($"Source file {sourceFile} missing.");
                        ++error;
                    }
                    continue;
                }
                if (SkipExisting && File.Exists(targetRoot + f.Directory + f.FileName)) {
                    _cfg.LogInfo($"Target {targetRoot}{f.Directory}{f.FileName} already exists, skipping.");
                    ++skipped;
                    continue;
                }
                _cfg.LogDebug($"{sDryRun}Copy {sourceFile} to {targetRoot}{f.Directory}{f.FileName}...");
                if (!dryRun) {
                    try {
                        Directory.CreateDirectory(targetRoot + f.Directory);
                    } catch (Exception e) {
                        _cfg.LogError($"failed to create target directory {targetRoot}{f.Directory}: " + e.Message);
                        ++error;
                        return new RunResult(count, error > 0, copied, skipped, error);
                    }
                }
                try {
                    MoveOrCopyFile(sourceFile, targetRoot + f.Directory + f.FileName, overwrite, move, dryRun);
                    ++copied;
                } catch (Exception e) {
                    _cfg.LogError($"failed to copy {sourceFile} to {targetRoot}{f.Directory}{f.FileName}: " + e.Message);
                    ++error;
                }
                ++count;
            }
            return new RunResult(count, error > 0, copied, skipped, error);
        }

        protected void MoveOrCopyFile(string sourceFileName, string destFileName, bool overwrite, bool move, bool dryRun) {
            if (dryRun && !File.Exists(sourceFileName))
                throw new FileNotFoundException("source file not found.", sourceFileName);
            if (dryRun)
                return;
            if (move) {
                if (overwrite && File.Exists(destFileName))
                    File.Delete(destFileName);
                File.Move(sourceFileName, destFileName);
            } else
                File.Copy(sourceFileName, destFileName, overwrite);
        }

        protected override void ValidateParams() {
            if (string.IsNullOrEmpty(TargetRoot))
                throw new ArgumentException("missing parameter --to");
        }

        public override bool ReadParam(string p, Config.ParamQueue<string> q) {
            switch (p) {
                case "--to":
                    TargetRoot = q.Dequeue();
                    if (!TargetRoot.PathEndsWithSeparator())
                        TargetRoot += "/";
                    return true;

                case "--overwrite":
                    Overwrite = true;
                    return true;

                case "--skip-existing":
                    SkipExisting = true;
                    return true;

                case "--move":
                    Move = true;
                    return true;

                case "--dry-run":
                    DryRun = true;
                    return true;

                default:
                    return false;
            }
        }

        public override IDictionary<string, string> ParamHelp => new Dictionary<string, string> {
            { "--to DIRECTORY", "target root directory" },
            { "--overwrite", "overwrite existing files in target" },
            { "--skip-existing", "skip existing files in target" },
            { "--move", "move files instead of copy" },
            { "--dry-run", "perform a trial run with no changes made" }
        };
    }
}
