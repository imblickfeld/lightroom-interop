﻿using imBlickFeld.Lightroom.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace imBlickFeld.Lightroom.Connect.Connectors
{
    class ListConnector : BaseConnector
    {
        public bool NullLineEndings { get; private set; }

        public override Config.Result Run() {
            ulong count = 0;

            using (var service = new CatalogService(_cfg.LRCatFilename, _cfg.CatalogServiceOptions)) {
                IEnumerable<LRFile> filenames;
                if (string.IsNullOrWhiteSpace(_cfg.Sql))
                    filenames = service.QueryFileNames(_cfg.Conditions, null, _cfg.OrderByFields, _cfg.IgnoreCase);
                else
                    filenames = service.QueryFileNames(_cfg.Sql);
                var res = filenames.Select(x => x.RootDirectory + x.Directory + x.FileName);
                if (_cfg.SkipMissing)
                    res = res.Where(x => File.Exists(x));
                foreach (var item in res) {
                    Console.Write(item);
                    if (NullLineEndings)
                        Console.Write('\0');
                    else
                        Console.WriteLine();
                    ++count;
                }
            }
            return count == 0 ? Config.Result.NoneSelected : Config.Result.AnySelected;
        }

        protected override RunResult DoRun(CatalogService service) {
            // Will never be called by this connector.
            throw new NotSupportedException();
        }

        public override bool ReadParam(string p, Config.ParamQueue<string> q) {
            switch (p) {
                case "-Z":
                case "--null":
                    NullLineEndings = true;
                    return true;

                default:
                    return false;
            }
        }

        protected override void ValidateParams() { }

        public override IDictionary<string, string> ParamHelp => new Dictionary<string, string> {
            { "-Z, --null", "print 0 byte character after each file name" }
        };
    }
}
