﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;

namespace imBlickFeld.Lightroom.Connect.Connectors
{
    abstract class WriteConnector : BaseConnector
    {
        public bool NoBackup { get; private set; }
        public bool DryRun { get; private set; }

        protected abstract RunResult DoRun(CatalogService service, SQLiteTransaction tran);

        public override bool ReadParam(string p, Config.ParamQueue<string> q) {
            switch (p) {
                case "--no-backup":
                    NoBackup = true;
                    return true;

                case "--dry-run":
                    DryRun = true;
                    NoBackup = true;
                    return true;

                default:
                    return base.ReadParam(p, q);
            }
        }

        public override Config.Result Run() {
            ValidateParams();
            bool dryRun = DryRun;
            if (!NoBackup) {
                _cfg.LogDebug($"Creating backup of {_cfg.LRCatFilename} to {_cfg.LRCatFilename}.bak, overwriting existing backup...");
                File.Copy(_cfg.LRCatFilename, _cfg.LRCatFilename + ".bak", true);
            }
            if (dryRun)
                _cfg.LogInfo("This is dry run: I will not change anything.");
            RunResult res;
            try {
                using (var service = new CatalogService(_cfg.LRCatFilename, _cfg.CatalogServiceOptions)) {
                    var tran = service.Connection.BeginTransaction();
                    res = DoRun(service, tran);
                    if (dryRun) {
                        _cfg.LogDebug("Discarting changes due to dry run...");
                        tran.Rollback();
                    } else {
                        _cfg.LogDebug("Saving changes...");
                        tran.Commit();
                    }
                }
            } finally {
                _cfg.WriteProgress(null);
            }
            LogResult(res);
            if (res.IsErr)
                return Config.Result.Error;
            return res.Count == 0 ? Config.Result.NoneSelected : Config.Result.AnySelected;
        }

        protected override RunResult DoRun(CatalogService service) {
            // Will never be called by this connector.
            throw new NotSupportedException();
        }

        public override IDictionary<string, string> ParamHelp => new Dictionary<string, string> {
            { "--no-backup", "do not backup LR catalog before write" },
            { "--dry-run", "perform a trial run with no changes made (implies no-backup)" }
        };
    }
}
