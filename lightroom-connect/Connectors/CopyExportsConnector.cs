﻿using imBlickFeld.Lightroom.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace imBlickFeld.Lightroom.Connect.Connectors
{
    class CopyExportsConnector : CopyFilesConnector
    {
        public string SourceDir { get; private set; }
        public string Extension { get; private set; }
        public bool IncludeCopies { get; private set; }

        public CopyExportsConnector() {
            var ph = base.ParamHelp;
            ph.Add("--source-dir DIRECTORY", "source directory");
            ph.Add("--ext EXTENSION", "change file extensions for source and target");
            ph.Add("--include-copies", "include exported copies such as \"filename-*.jpg\"");
            ParamHelp = ph;
        }

        protected override RunResult DoRun(CatalogService service) {
            ulong count = 0, copied = 0, skipped = 0, error = 0;

            string sourceDir = SourceDir;
            string targetRoot = TargetRoot;
            bool overwrite = Overwrite;
            bool move = Move;
            bool skipMissing = _cfg.SkipMissing;
            bool dryRun = DryRun;
            string sDryRun = dryRun ? "[Dry Run] " : "";
            List<string> files = new List<string>();

            IList<LRFile> filenames;
            if (string.IsNullOrWhiteSpace(_cfg.Sql))
                filenames = service.QueryFileNames(_cfg.Conditions, null, _cfg.OrderByFields, _cfg.IgnoreCase).ToList();
            else
                filenames = service.QueryFileNames(_cfg.Sql).ToList();
            int max = filenames.Count;
            int i = 0;
            foreach (var f in filenames) {
                _cfg.WriteProgress(i++, max);
                string fn = Extension == null ? f.FileName : Path.ChangeExtension(f.FileName, Extension);
                files.Clear();
                files.Add(fn);
                if (IncludeCopies) {
                    files.AddRange(
                        Directory.GetFiles(sourceDir, Path.GetFileNameWithoutExtension(fn) + "-*" + Path.GetExtension(fn), SearchOption.TopDirectoryOnly)
                                    .Select(x => Path.GetFileName(x)));
                }
                foreach (var fileName in files) {
                    if (!File.Exists(sourceDir + fileName)) {
                        if (skipMissing) {
                            _cfg.LogDebug($"Skipping missing source file {sourceDir}{fileName}.");
                            ++skipped;
                        } else {
                            _cfg.LogError($"Source file {sourceDir}{fileName} missing.");
                            ++error;
                        }
                        continue;
                    }
                    if (SkipExisting && File.Exists(targetRoot + f.Directory + fileName)) {
                        _cfg.LogInfo($"Target {targetRoot}{f.Directory}{fileName} already exists, skipping.");
                        ++skipped;
                        continue;
                    }
                    _cfg.LogDebug($"{sDryRun}Copy {sourceDir}{fileName} to {targetRoot}{f.Directory}{fileName}...");
                    if (!dryRun) {
                        try {
                            Directory.CreateDirectory(targetRoot + f.Directory);
                        } catch (Exception e) {
                            _cfg.LogError($"failed to create target directory {targetRoot}{f.Directory}: " + e.Message);
                            ++error;
                            return new RunResult(count, error > 0, copied, skipped, error);
                        }
                    }
                    try {
                        MoveOrCopyFile(sourceDir + fileName, targetRoot + f.Directory + fileName, overwrite, move, dryRun);
                        ++copied;
                    } catch (Exception e) {
                        _cfg.LogError($"failed to copy {sourceDir}{fileName} to {targetRoot}{f.Directory}{fileName}: " + e.Message);
                        ++error;
                    }
                    ++count;
                }
            }
            return new RunResult(count, error > 0, copied, skipped, error);
        }

        protected override void ValidateParams() {
            if (string.IsNullOrEmpty(SourceDir))
                throw new ArgumentException("missing parameter --source-dir");
            base.ValidateParams();
        }

        public override bool ReadParam(string p, Config.ParamQueue<string> q) {
            switch (p) {
                case "--source-dir":
                    SourceDir = q.Dequeue();
                    if (!SourceDir.PathEndsWithSeparator())
                        SourceDir += "/";
                    return true;
                
                case "--ext":
                    Extension = q.Dequeue();
                    return true;

                case "--include-copies":
                    IncludeCopies = true;
                    return true;

                default:
                    return base.ReadParam(p, q);
            }
        }
        public override IDictionary<string, string> ParamHelp { get; }
    }
}
