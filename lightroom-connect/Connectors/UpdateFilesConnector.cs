﻿using imBlickFeld.Lightroom.Connect.Connectors.FileUpdaters;
using imBlickFeld.Lightroom.Connect.Connectors.Extractors;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using imBlickFeld.Lightroom.Entities;
using imBlickFeld.Lightroom.Connect.Connectors.Extractors.RegexReplaceCommands;

namespace imBlickFeld.Lightroom.Connect.Connectors
{
    class UpdateFilesConnector : WriteConnector
    {
        public IList<string> UpdatePatternsRaw { get; private set; }
        public IDictionary<IFileUpdater, IList<IFieldExtractor>> UpdatePatterns { get; private set; }

        public UpdateFilesConnector() {
            var ph = base.ParamHelp;
            ph.Add("--to PATTERN", "pattern for target update (multiple allowed)");
            ph.Add("z00", @"
    PATTERN:

    A pattern specifying what to update and how:

    Syntax:
        <PATTERN>         = <TARGET>=<SOURCES>
        <TARGET>          = "
                + FileUpdaterFactory.I.GetUpdaterHelp()
                        .OrderBy(x => x.Key)
                        .Select(x => $"{x.Key,-14}  {x.Value}")
                        .ToString($"\n{" ",-26}| ")
                + @"
        <SOURCES>         = <one or more SOURCE>
        <SOURCE>          = <LITERAL> | {<REGEX-EXTRACTOR>}
        <LITERAL>         = <any text not containing {>
        <REGEX-EXTRACTOR> = <SEP><FIELD><SEP><REGEX><SEP><REPLACE-DEFS>
        <SEP>             = <any separator character>
        <FIELD>           = "
                + RegexFieldExtractor.FieldHelpText
                        .OrderBy(x => x.Key)
                        .Select(x => $"{x.Key,-14}  {x.Value}")
                        .ToString($"\n{" ",-26}| ")
                + @"
        <REGEX>           = <any valid regular expression to extract values from FIELD>
        <REPLACE-DEFS>    = <one or more REPLACE-DEF>
        <REPLACE-DEF>     = <REGEX-GROUP>
                          | "
                + RegexReplaceCommandFactory.I.HelpText
                        .OrderBy(x => x.Syntax)
                        .Select(x => $"{x.Syntax}\n{" ",-32}{x.HelpText}")
                        .ToString($"\n{" ",-26}| ")
                + @"
        <REGEX-GROUP>     = \<DIGITS>       any group matched by <REGEX>
                                            any <REPLACE-DEF> parameter may contain one or more <REGEX-GROUP>

    Example:

        Rename all files in directory ""2020.001 whatever"".
        Assuming the files are named e.g. IMG_1234.jpg or IMG_2345a Title.jpg, the resulting filenames shall be:
            IMG_1234.jpg        --> 2020.001.001.jpg
            IMG_2345a Title.jpg --> 2020.001.002.B1.jpg
        while the last numeric part of the file name shall be a sequence grouped by the numeric part of the original filename.
        Furthermore the title part in the filename shall be written as the file's title metadata.

        lightroom-connect updatefiles ^
            --from myCatalog.lrcat ^
            --and directory like ""%2020.001%/"" ^
            --to ""filename={!directory!(\d{4}\.\d{3})[^/]*/$!\1}.{!basename!^(\S{3}_\d{4})([a-z])?!\sequence-distinct(\1)(1)(3)\replace-if(\2)(!a!b!c!d)(!.B1!.B2!.B3!.B4)}.{!ext!(.*)!\tolower(\1)}"" ^
            --to ""title={!basename!^\S{3}_\d{4}[a-z]?\W+(.*)$!\1}"" ^

        lightroom-connect updatefiles       run lightroom-connect in updatefiles mode
        --from myCatalog.lrcat              use LR catalog ""myCatalog.lrcat""
        --and directory like ""%2020.001%/""  select files from directory matching ""*2020.001*""
        --to ""filename=                     update all selected filenames to:
            {!directory!(\d{4}\.\d{3})[^/]*/$!\1}
                                            start with the ""2020.001"" part of the directory name,
            .                               append a ""."",
            {!basename!^(\S{3}_\d{4})([a-z])?!
                                            match ""IMG_1234a"" part of the filename to ""\1\2"",
                \sequence-distinct(\1)(1)(3)
                                            append a sequence distingished by the ""IMG_1234"" part
                                            (starting with 1 and padded by zeros to 3 digits),
                \replace-if(\2)(!a!b!c!d)(!.B1!.B2!.B3!.B4)
                                            append "".B1"" if the 2nd part is ""a""
                                            or append "".B2"" if the 2nd part is ""b""
                                            or append "".B3"" if the 2nd part is ""c""
                                            or append "".B4"" if the 2nd part is ""d""
            }
            .                               append a ""."",
            {!ext!(.*)!\tolower(\1)}        append the lowercase file extension
        ""
        --to ""title=                        update the title of all selected files to:
            {!basename!^\S{3}_\d{4}[a-z]?\W+(.*)$!\1}
                                            the ""Title"" part in ""IMG_1234a Title.jpg""
        """);
            ParamHelp = ph;
            UpdatePatternsRaw = new List<string>();
        }

        protected override RunResult DoRun(CatalogService service, SQLiteTransaction tran) {
            bool dryRun = DryRun;
            string sDryRun = dryRun ? "[Dry Run] " : "";
            bool isErr = false;
            ulong count = 0;
            ulong successful = 0;
            ulong skipped = 0;
            ulong withError = 0;
            foreach (var updater in UpdatePatterns)
                updater.Key.Init(service);
            IList<LRFile> filenames;
            if (string.IsNullOrWhiteSpace(_cfg.Sql))
                filenames = service.QueryFileNames(_cfg.Conditions, null, _cfg.OrderByFields, _cfg.IgnoreCase).ToList();
            else
                filenames = service.QueryFileNames(_cfg.Sql).ToList();
            int max = filenames.Count;
            int i = 0;
            foreach (var f in filenames) {
                _cfg.WriteProgress(i++, max);
                bool localError = false;
                bool fileSkipped = false;
                foreach (var pair in UpdatePatterns) {
                    _cfg.LogDebug($"{sDryRun}Updating {pair.Key.FieldName} of {f.RootDirectory}{f.Directory}{f.FileName}...");
                    try {
                        if (pair.Key.Run(f, pair.Value, service, dryRun))
                            fileSkipped = true;
                    } catch (Exception e) {
                        localError = true;
                        isErr = true;
                        _cfg.LogError($"failed to update {pair.Key.FieldName} of {f.RootDirectory}{f.Directory}{f.FileName}: " + e.Message);
                    }
                }
                if (localError)
                    ++withError;
                else {
                    ++successful;
                    if (fileSkipped)
                        ++skipped;
                }
                ++count;
            }
            return new RunResult(count, isErr, successful, skipped, withError);
        }

        public override bool ReadParam(string p, Config.ParamQueue<string> q) {
            switch (p) {
                case "--to":
                    UpdatePatternsRaw.Add(q.Dequeue());
                    return true;

                default:
                    return base.ReadParam(p, q);
            }
        }

        protected override void ValidateParams() {
            var extractorFactory = FieldExtractorFactory.I;
            var updaterFactory = FileUpdaterFactory.I;
            UpdatePatterns = UpdatePatternsRaw
                .Select(x => x.Split(new[] { '=' }, 2))
                .ToDictionary(x => updaterFactory.GetUpdater(x[0]), x => {
                    if (x.Length < 2) throw new ArgumentException($"Update definition missing for \"{x[0]}\".");
                    return extractorFactory.FromString(x[1]);
                });
            UpdatePatternsRaw = null;
        }

        public override IDictionary<string, string> ParamHelp { get; }
    }
}
