﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors
{
    class InvalidConnector : IConnector
    {
        public IDictionary<string, string> ParamHelp => throw new InvalidOperationException("No mode selected.");

        public bool ReadParam(string p, Config.ParamQueue<string> q) {
            return false;
        }

        public Config.Result Run() {
            throw new InvalidOperationException("No mode selected.");
        }
    }
}
