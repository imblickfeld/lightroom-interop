﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect.Connectors
{
    abstract class BaseConnector : IConnector
    {
        protected readonly Config _cfg = Config.I;

        public virtual Config.Result Run() {
            ValidateParams();
            RunResult res;
            try {
                using (var service = new CatalogService(_cfg.LRCatFilename, _cfg.CatalogServiceOptions)) {
                    res = DoRun(service);
                }
            } finally {
                _cfg.WriteProgress(null);
            }
            LogResult(res);
            if (res.IsErr)
                return Config.Result.Error;
            return res.Count == 0 ? Config.Result.NoneSelected : Config.Result.AnySelected;
        }

        protected void LogResult(RunResult res) {
            _cfg.LogInfo($@"
{res.Count} file{(res.Count == 1 ? "" : "s")} processed, 
{res.FilesSuccessful} file{(res.FilesSuccessful == 1 ? "" : "s")} successful,
{res.FilesSkipped} file{(res.FilesSkipped == 1 ? "" : "s")} skipped,
{res.FilesWithError} file{(res.FilesWithError == 1 ? "" : "s")} had errors.");
        }

        protected abstract RunResult DoRun(CatalogService service);
        protected abstract void ValidateParams();

        public virtual bool ReadParam(string p, Config.ParamQueue<string> q) {
            return false;
        }

        private static readonly IDictionary<string, string> EMPTY_HELP = new Dictionary<string, string>(0);
        public virtual IDictionary<string, string> ParamHelp => EMPTY_HELP;

        protected class RunResult
        {
            public readonly ulong Count;
            public readonly bool IsErr;
            public readonly ulong FilesSuccessful;
            public readonly ulong FilesSkipped;
            public readonly ulong FilesWithError;

            public RunResult(ulong count, bool isErr, ulong filesSuccessul, ulong filesSkipped, ulong filesWithError) {
                Count = count;
                IsErr = isErr;
                FilesSuccessful = filesSuccessul;
                FilesSkipped = filesSkipped;
                FilesWithError = filesWithError;
            }
        }
    }
}
