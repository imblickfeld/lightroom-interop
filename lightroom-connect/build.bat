@echo off
set CURDIR=%CD%
set TARGETDIR=..\release
set OBJDIR=bin\Release
set PROJECTNAME=lightroom-connect

if not defined VS_PATH set "VS_PATH=c:\Programs\Compiler\VS2019\Common7\"
call "%VS_PATH%Tools\VsDevCmd.bat"
rem VsDevCmd.bat scheint den aktuellen Pfad zu �ndern...
cd /d "%CURDIR%"

del /s /q /f %OBJDIR%\*.*
del /s /q /f %TARGETDIR%\*.*

msbuild %PROJECTNAME%.csproj /p:Configuration=Release /p:Platform="Any CPU" /p:OutputPath=%OBJDIR% || goto Error

mkdir %TARGETDIR%
cd %OBJDIR%
cmd /c 7za a -tzip -r -x!*.pdb -x!*.xml ..\..\%TARGETDIR%\%PROJECTNAME%.zip .
cd "%CURDIR%"

del /s /q "%OBJDIR%\*.*"

echo Done.
goto :EOF

:Error
echo Failed.