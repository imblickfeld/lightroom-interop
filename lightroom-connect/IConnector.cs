﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect
{
    interface IConnector
    {
        Config.Result Run();

        bool ReadParam(string p, Config.ParamQueue<string> q);

        IDictionary<string, string> ParamHelp { get; }
    }
}
