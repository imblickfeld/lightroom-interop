﻿using imBlickFeld.Lightroom.Conditions;
using System;

namespace imBlickFeld.Lightroom.Connect
{
    static class ConditionFactory
    {
        public static Condition CreateCondition(string name, Operator op, string value) {
            switch (name.ToLowerInvariant()) {
                case "camera-model":
                    return new CameraModel(op, value);
                case "camera-sn":
                    return new CameraSN(op, value);
                case "city":
                    return new City(op, value);
                case "collection":
                    return new Collection(op, value);
                case "copyright":
                    return new Copyright(op, value);
                case "country":
                    return new Country(op, value);
                case "creator":
                    return new Creator(op, value);
                case "date":
                    return new Date(op, value);
                case "directory":
                    return new Directory(op, value);
                case "focal-length":
                    return new FocalLength(op, int.Parse(value));
                case "image-rating":
                    return new ImageRating(op, int.Parse(value));
                case "iso":
                    return new IsoSpeedRating(op, int.Parse(value));
                case "lens":
                    return new Lens(op, value);
                case "location":
                    return new Location(op, value);
                case "shutter-speed":
                    return new ShutterSpeed(op, value);
                case "tag":
                    return new Tag(op, value);
                default:
                    throw new ArgumentException(@"Invalid condition: {p}. Valid conditions are:
    - camera-model
    - camera-sn
    - city
    - collection
    - copyright
    - country
    - creator
    - date
    - directory
    - focal-length
    - image-rating
    - iso
    - lens
    - location
    - shutter-speed
    - tag");
            }
        }

        public static string HelpText =>
            @"    collection              image collection name
    camera-model            camera model name
    camera-sn               camera serial number
    city                    IPTC city
    collection              image collection
    copyright               image copyright text
    country                 IPTC country
    creator                 IPTC creator
    date                    image date formatted as ""YYYY-MM-DD""
    directory               file source directory
    focal-length            focal length in mm
    image-rating            image rating in stars (VALUE is 0..5)
    iso                     ISO speed
    lens                    lens name
    location                IPTC location
    shutter-speed           shutter speed in seconds, e.g. ""1/200""
    tag                     associated keyword";
    }
}
