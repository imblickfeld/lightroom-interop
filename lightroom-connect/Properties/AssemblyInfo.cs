﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("lightroom-connect")]
[assembly: AssemblyDescription("Command line interface to query Adobe Photoshop Lightroom catalogs.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("software.imBlickFeld.de")]
[assembly: AssemblyProduct("lightroom-connect")]
[assembly: AssemblyCopyright("Copyright © 2021 by Klaus Bergmann")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1de36c1f-9c1a-459f-8b30-03180bd4869d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.6.0.0")]
[assembly: AssemblyFileVersion("1.6.0.0")]
[assembly: NeutralResourcesLanguage("en")]
