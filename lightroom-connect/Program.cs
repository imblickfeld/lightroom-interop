﻿using System;
using System.Linq;

namespace imBlickFeld.Lightroom.Connect
{
    class Program
    {
        static int Main(string[] args) {
            try {
                try {
                    Config.I.Init(args);
                } catch (ArgumentException e) {
                    ShowHelp(e);
                    return 1;
                }
                if (Config.I.ShowHelp) {
                    ShowHelp();
                    return 0;
                }
                if (Config.I.ShowVersion) {
                    ShowVersion();
                    return 0;
                }
                try {
                    return (int)Config.I.Connector.Run();
                } catch (Exception e) {
                    Config.I.LogError(e);
                    return (int)Config.Result.Error;
                }
            }  finally {
                if (Config.I.Pause) {
                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey();
                }
            }
        }

        public static void ShowHelp(Exception e) {
            if (e != null)
                Console.Error.WriteLine(Config.I.AppName + ": " + e.Message);
            Console.Error.WriteLine($@"Usage: {Config.I.AppName} MODE OPTION...
Try '{Config.I.AppName} --help' for more information.");
        }

        public static void ShowHelp() {
            Console.WriteLine($@"Usage: {Config.I.AppName} MODE OPTION...
Query a Lightroom catalog.
Example: {Config.I.AppName} list --from my.lrcat --and image-rating "">"" 2 --and image-rating ""<="" 4

MODE:

{((Mode[])Enum.GetValues(typeof(Mode))
     .Cast<Mode>())
     .Where(x => x != Mode.Undefined)
     .Select(x => $"    {x.Name(),-22}  {x.Help()}")
     .ToString("\n")}


OPTION:

Pattern selection and interpretation:
    -f, --from              Lightroom catalog to query
        --and               new AND CONDITION
        --or                new OR CONDITION
        --order-by          new ORDER-BY-FIELD
        --sql               full SQL query
                                When given, --and, --or, --order-by and --ignore-case will be ignored.
                                The selected fields must contain at least:
                                    AgLibraryFile.id_local (must be the first field),
                                    AgLibraryRootFolder.absolutePath,
                                    AgLibraryFolder.pathFromRoot,
                                    AgLibraryFile.baseName,
                                    AgLibraryFile.extension,
                                    AgLibraryFile.sidecarExtensions
    -i, --ignore-case       ignore case on string comparison
        --skip-missing      skip missing source files

Miscellaneous:
    -V, --version           display version information and exit
        --help              display this help text and exit

Output control:
    -p, --pause             pause for key on exit
    -P, --no-pause          do not pause for key on exit
    -q, --quiet, --silent   suppress all normal output
    -s, --no-messages       suppress error messages
    -v, --verbose           verbose output
    -l, --log-file FILE     redirect output to FILE
        --log-append        append to log file

{((Mode[])Enum.GetValues(typeof(Mode)))
     .Where(x => x != Mode.Undefined)
     .Select(x => new { Mode = x, Connector = x.CreateConnector() })
     .Select(x => new { x.Mode, x.Connector, x.Connector.ParamHelp })
     .Where(x => x.Connector.ParamHelp.Count > 0)
     .Select(x => $"Only for mode {x.Mode.Name()}:\n"
            + x.ParamHelp.OrderBy(p => p.Key)
                         .Select(p => p.Key.StartsWith("z") ? p.Value : $"    {(p.Key.StartsWith("--") ? "    " : "") + p.Key,-22}  {p.Value}")
                         .ToString("\n"))
     .ToString("\n\n")}


CONDITION:

Any condition has the format: NAME OPERATOR VALUE

Valid NAMEs are:
{ConditionFactory.HelpText}

Valid OPERATORs are:
    {Config.OPERATORS.Keys.ToString(", ")}

ORDER-BY-FIELD:

Any field the filenames will be sorted by
Valid fields are:
    {Config.ORDER_BY_VALUES.Keys.ToString(", ")}


Exit status is 0 if any line is selected, 1 otherwise;
if any error occurs the exit status is 2.");
        }

        public static void ShowVersion() {
            Console.WriteLine($@"lightroom-connect v{typeof(Program).Assembly.GetName().Version}
Copyright (C) 2021 software.imBlickFeld.de
License GPLv3: GNU GPL version 3 <http://www.gnu.org/licenses/gpl-3.0>.
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions.

This software contains the following third-party software:
- EntityFramework by Microsoft, licensed under Apache 2.0 License
- MSTest Framework by Microsoft, licensed under MIT License
- SQLite, given to the Public Domain

Written by Klaus Bergmann, see <https://www.imblickfeld.de/contact>.");
        }
    }
}
