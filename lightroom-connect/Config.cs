﻿using imBlickFeld.Lightroom.Conditions;
using imBlickFeld.Lightroom.Connect.Connectors;
using imBlickFeld.Lightroom.Loggers;
using imBlickFeld.Lightroom.OrderBys;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Connect
{
    class Config : ILogger, IDisposable
    {
        public static readonly Config I = new Config();

        /// <summary>Application exe name (most probably "lightroom-connect")</summary>
        public readonly string AppName;

        public Mode Mode { get; private set; }

        public IConnector Connector { get; private set; }

        /// <summary>Lightroom catalog filename to query. Wildcards or directories are not supported.</summary>
        public string LRCatFilename { get; private set; }

        public CatalogServiceOptions CatalogServiceOptions { get; }

        public string Sql { get; private set; }

        public Condition Conditions { get; private set; }

        public IList<OrderByField> OrderByFields { get; private set; }

        /// <summary>Ignore case on string comparison.</summary>
        public bool IgnoreCase { get; private set; }

        /// <summary>skip missing source files</summary>
        public bool SkipMissing { get; private set; }

        /// <summary>Suppress error messages.</summary>
        public bool SuppressErrorMessages { get; private set; }

        /// <summary>Suppress normal output.</summary>
        public bool Quiet { get; private set; }

        /// <summary>Level of debug output</summary>
        public VerboseLevel Verbose { get; private set; }

        /// <summary>Log file to redirect output to</summary>
        public string LogFile { get; private set; }
        public TextWriter LogWriter { get; private set; }

        /// <summary>Append to log file</summary>
        public bool LogAppend { get; private set; }

        /// <summary>Display help text and exit.</summary>
        public bool ShowHelp { get; private set; }

        /// <summary>Display version information and exit</summary>
        public bool ShowVersion { get; private set; }

        /// <summary>Pause for key on exit if <c>true</c>.</summary>
        public bool Pause { get; private set; }

        public bool IsConnectorRequired { get; private set; }

        private Config() {
            AppName = AppDomain.CurrentDomain.FriendlyName;
            Mode = Mode.Undefined;
            CatalogServiceOptions = new CatalogServiceOptions();
            OrderByFields = new List<OrderByField>(1);
            IsConnectorRequired = true;
        }

        public void Init(string[] args) {
            var q = new ParamQueue<string>(args);

            if (q.Count == 0) {
                ShowHelp = true;
                return;
            }

            var p = q.Dequeue();
            KeyValuePair<string, Exception>? invalidMode = null;
            try {
                Mode = (Mode)Enum.Parse(typeof(Mode), p, true);
                Connector = Mode.CreateConnector();
            } catch (Exception e) {
                invalidMode = new KeyValuePair<string, Exception>(p, e);
                Connector = new InvalidConnector();
                q.Push(p);
            }
            
            bool loop = true;
            while (loop && q.Count > 0) {
                p = q.Dequeue();

                // explode "stuffed" single-char arguments such as: -abc ==> -a -b -c
                if (p.Length > 2 && p[0] == '-' && p[1] != '-') {
                    foreach (char ch in p.Substring(1))
                        q.Push("-" + ch);
                    continue;
                }

                if (p.StartsWith("--") && p.Contains('=')) {
                    var parts = p.Split(new[] { '=' }, 2);
                    q.Push(parts[1]);
                    q.Push(parts[0]);
                    continue;
                }

                try {
                    bool done = false;
                    done = done || ReadParamHelp(p, q);
                    done = done || ReadParamSelection(p, q);
                    done = done || ReadParamMisc(p, q);
                    done = done || ReadParamOutputControl(p, q);
                    done = done || Connector.ReadParam(p, q);
                    
                    if (!done)
                        throw new ArgumentException("unknown parameter " + p);
                } catch (Exception e) {
                    throw new ArgumentException(GetErrorMessage(p, e), e);
                }
            }
            if (!IsConnectorRequired)
                return;
            if (invalidMode.HasValue) {
                throw new ArgumentException($"Invalid mode: {invalidMode.Value.Key}. Valid modes are:\n" +
                    Enum.GetNames(typeof(Mode))
                        .Where(x => x != Mode.Undefined.ToString())
                        .Select(x => "    - " + x.ToLowerInvariant())
                        .ToString("\n"), invalidMode.Value.Value);
            }
            if (LRCatFilename == null)
                throw new ArgumentException("No Lightroom catalog specified.");
            if (LogFile != null)
                LogWriter = new StreamWriter(LogFile, LogAppend);
            if (OrderByFields.Count == 0)
                OrderByFields.Add(OrderBy.CreationTime.ToOrderByField());
        }

        private bool ReadParamHelp(string p, ParamQueue<string> q) {
            switch (p) {
                case "-V":
                case "--version":
                    ShowVersion = true;
                    IsConnectorRequired = false;
                    return true;

                case "--help":
                    ShowHelp = true;
                    IsConnectorRequired = false;
                    return true;

                default:
                    return false;
            }
        }

        private bool ReadParamSelection(string p, ParamQueue<string> q) {
            var nextConditionType = NextConditionType.None;
            switch (p) {
                // Pattern selection and interpretation:
                case "-f":
                case "--from":
                    LRCatFilename = q.Dequeue();
                    return true;

                case "--skip-missing":
                    SkipMissing = true;
                    return true;

                case "--sql":
                    Sql = q.Dequeue();
                    return true;

                case "--and":
                    nextConditionType = NextConditionType.And;
                    goto case "and-or";
                case "--or":
                    if (Conditions == null && p == "--or")
                        throw new ArgumentException("The first condition must be --and.");
                    nextConditionType = NextConditionType.Or;
                    goto case "and-or";
                case "and-or": // case for both --and and --or:
                    var conditionName = q.Dequeue();
                    Operator op = ParseOperator(q.Dequeue());
                    var value = op == Operator.IsNull || op == Operator.IsNotNull ? null : q.Dequeue();
                    var condition = ConditionFactory.CreateCondition(conditionName, op, value);
                    if (Conditions == null)
                        Conditions = condition;
                    else
                        Conditions.Add(nextConditionType, condition);
                    return true;
                case "--order-by":
                    OrderByFields.Add(ParseEnum<OrderBy>(q.Dequeue(), ORDER_BY_VALUES).ToOrderByField());
                    return true;
                case "-i":
                case "--ignore-case":
                    IgnoreCase = true;
                    return true;

                default:
                    return false;
            }
        }

        public static readonly Dictionary<string, Operator> OPERATORS = ((Operator[])Enum.GetValues(typeof(Operator)))
            .Select(x => new { Key = x.ToString().ToLowerInvariant(), Value = x })
            .Union(new Dictionary<string, Operator> {
                { "=", Operator.Eq },
                { "!=", Operator.Neq },
                { "<>", Operator.Neq },
                { ">", Operator.Gt },
                { ">=", Operator.Geq },
                { "<", Operator.Lt },
                { "<=", Operator.Leq },
            }.Select(x => new { x.Key, x.Value }))
            .ToDictionary(x => x.Key, x => x.Value);

        private Operator ParseOperator(string p) {
            if (!OPERATORS.TryGetValue(p, out Operator result))
                throw new ArgumentException($"Invalid operator: {p}. Valid operators are:\n" +
                    OPERATORS.Keys
                        .OrderBy(x => x)
                        .Select(x => "    - " + x.ToLowerInvariant())
                        .ToString("\n"));
            return result;
        }

        public static readonly Dictionary<string, OrderBy> ORDER_BY_VALUES = ((OrderBy[])Enum.GetValues(typeof(OrderBy)))
                .Select(x => new { Key = x.ToString().ToLowerInvariant(), Value = x })
                .ToDictionary(x => x.Key, x => x.Value);

        private T ParseEnum<T>(string p, IDictionary<string, T> supportedValues = null) where T : struct, IConvertible {
            var values = supportedValues ?? ((T[])Enum.GetValues(typeof(T)))
                .Select(x => new { Key = x.ToString().ToLowerInvariant(), Value = x })
                .ToDictionary(x => x.Key, x => x.Value);
            if (!values.TryGetValue(p, out T result))
                throw new ArgumentException($"Invalid value: {p}. Valid values are:\n" +
                    values.Keys
                        .OrderBy(x => x)
                        .Select(x => "    - " + x.ToLowerInvariant())
                        .ToString("\n"));
            return result;
        }

        private bool ReadParamMisc(string p, ParamQueue<string> q) {
            switch (p) {
                // Miscellaneous:
                default:
                    return false;
            }
        }

        private bool ReadParamOutputControl(string p, ParamQueue<string> q) {
            switch (p) {
                // Output control:
                case "-p":
                case "--pause":
                    Pause = true;
                    return true;

                case "-P":
                case "--no-pause":
                    Pause = false;
                    return true;

                case "-q":
                case "--quiet":
                case "--silent":
                    Quiet = true;
                    return true;

                case "-s":
                case "--no-messages":
                    SuppressErrorMessages = true;
                    return true;

                case "-v":
                case "--verbose":
                    switch (Verbose) {
                        case VerboseLevel.Info:
                            // -v
                            Verbose = VerboseLevel.Debug;
                            CatalogServiceOptions.DebugLogger = this;
                            break;
                        case VerboseLevel.Debug:
                            // -vv
                            Verbose = VerboseLevel.Trace;
                            CatalogServiceOptions.TraceLogger = this;
                            break;
                        default:
                            break;
                    }
                    return true;

                case "-l":
                case "--log-file":
                    LogFile = q.Dequeue();
                    return true;

                case "--log-append":
                    LogAppend = true;
                    return true;

                default:
                    return false;
            }
        }

        private string GetErrorMessage(string p, Exception e) {
            var fnfe = e as FileNotFoundException;
            if (fnfe != null)
                return fnfe.FileName + ": No such file or directory";
            if (e is InvalidOperationException)
                return "option requires an argument -- " + p;
            return e.Message;
        }

        public void LogError(string msg, Exception e = null) {
            if (!SuppressErrorMessages) {
                if (LogWriter == null)
                    Console.Error.WriteLine($"{AppName}: {msg.Trim()}");
                else
                    LogWriter.WriteLine("Error: " + msg.Trim());
                if (e != null && Verbose != VerboseLevel.Info)
                    LogError(e.ToString());
            }
        }

        public void LogError(Exception e) {
            LogError(Verbose != VerboseLevel.Info ? e.ToString() : e.Message);
        }

        public void LogInfo(string msg = "") {
            if (!Quiet)
                (this as ILogger).Log(msg.Trim());
        }

        public void LogDebug(string msg = "") {
            if (Verbose != VerboseLevel.Info)
                (this as ILogger).Log("Debug: " + msg.Trim());
        }

        public void LogTrace(string msg = "") {
            if (Verbose == VerboseLevel.Trace)
                (this as ILogger).Log("Trace: " + msg.Trim());
        }

        void ILogger.Log(string msg) {
            if (LogWriter == null)
                Console.WriteLine(msg);
            else
                LogWriter.WriteLine(msg);
        }

        public void WriteProgress(int? x, int max = 100) {
            if (LogWriter == null)  // write progress only if logging to file.
                return;
            if (x == null) {
                WriteBottomLine(new string(' ', Console.WindowWidth - 1));
                return;
            }
            int i = max == 100 ? x.Value : (x.Value * 100) / max;
            WriteBottomLine($"{i,3} % " + new string('\u2588', i * (Console.WindowWidth - 6) / 100));
        }

        private void WriteBottomLine(string msg, bool eraseLineFirst = false) {
            if (Console.IsOutputRedirected)
                return;
            Console.CursorVisible = false;
            try {
                var p = new CursorPos(Console.CursorLeft, Console.CursorTop);
                string emptyLine = new string(' ', Console.WindowWidth - 1);
                if (p.Top == Console.WindowTop + Console.WindowHeight - 1) {
                    // we're already on the bottom line. Scroll one more line.
                    Console.SetCursorPosition(0, p.Top);
                    Console.Write(emptyLine);
                    eraseLineFirst = false;
                    Console.WriteLine();
                }
                if (eraseLineFirst) {
                    Console.SetCursorPosition(0, Console.WindowTop + Console.WindowHeight - 1);
                    Console.Write(emptyLine);
                }
                Console.SetCursorPosition(0, Console.WindowTop + Console.WindowHeight - 1);
                Console.Write(msg);
                Console.SetCursorPosition(p.Left, p.Top);
            } finally {
                Console.CursorVisible = true;
            }
        }

        public void Dispose() {
            if (LogWriter != null)
                LogWriter.Dispose();
        }

        #region public enum Result...
        public enum Result
        {
            AnySelected = 0,
            NoneSelected = 1,
            Error = 2
        }
        #endregion

        #region private struct CursorPos...
        private struct CursorPos
        {
            public readonly int Left;
            public readonly int Top;

            public CursorPos(int left, int top) {
                Left = left;
                Top = top;
            }
        }
        #endregion

        #region class ParamQueue...
        public class ParamQueue<T> : LinkedList<T>
        {
            public ParamQueue(IEnumerable<T> args) : base(args) { }
            public T Dequeue() {
                var res = First;
                RemoveFirst();
                return res == null ? default(T) : res.Value;
            }

            public void Enqueue(T value) {
                AddLast(value);
            }

            public void Push(T value) {
                AddFirst(value);
            }
        } 
#endregion
    }

    enum VerboseLevel
    {
        Info = 0,
        Debug = 1,
        Trace = 2
    }

    enum Mode
    {
        Undefined = 0,
        List = 1,
        CopyFiles = 2,
        CopyExports = 3,
        UpdateFiles = 4
    }

    static class ModeExtensions
    {
        public static IConnector CreateConnector(this Mode mode) {
            switch (mode) {
                case Mode.Undefined:
                    throw new InvalidOperationException("No mode selected.");
                case Mode.List:
                    return new ListConnector();
                case Mode.CopyFiles:
                    return new CopyFilesConnector();
                case Mode.CopyExports:
                    return new CopyExportsConnector();
                case Mode.UpdateFiles:
                    return new UpdateFilesConnector();
                default:
                    throw new NotImplementedException($"Mode {mode.ToString().ToLowerInvariant()} is not yet supported.");
            }
        }

        public static string Name(this Mode mode) {
            return mode.ToString().ToLowerInvariant();
        }
        public static string Help(this Mode mode) {
            switch (mode) {
                case Mode.List:
                    return "list selected files";
                case Mode.CopyFiles:
                    return "copy selected files";
                case Mode.CopyExports:
                    return "copy (exported) files based on selection";
                case Mode.UpdateFiles:
                    return "rename selected files";
                default:
                    return $"unknown mode {mode.ToString().ToLowerInvariant()}";
            }
        }
    }
}
