# Lightroom Interop

Interop class for Adobe Photoshop Lightroom including command line interface to query catalogs.

# LRcat database documentation:

* [Database Schema](https://imblickfeld.gitlab.io/lightroom-interop/doc/schema-lrcat5/)
* [Lightroom Unofficial Database Table Reference](https://github.com/camerahacks/lightroom-database)
* [Dive into Lightroom catalogues](https://www.seachess.net/notes/dive-into-lightroom-catalogues/)
* [LRcat format](https://github.com/hfiguiere/lrcat-extractor/blob/master/doc/lrcat_format.md)

# Lightroom-Connect

````
Usage: lightroom-connect.exe MODE OPTION...
Query a Lightroom catalog.
Example: lightroom-connect.exe list --from my.lrcat --and image-rating ">" 2 --and image-rating "<=" 4

MODE:

    list                    list selected files
    copyfiles               copy selected files
    copyexports             copy (exported) files based on selection
    updatefiles             rename selected files


OPTION:

Pattern selection and interpretation:
    -f, --from              Lightroom catalog to query
        --and               new AND CONDITION
        --or                new OR CONDITION
        --order-by          new ORDER-BY-FIELD
        --sql               full SQL query
                                When given, --and, --or, --order-by and --ignore-case will be ignored.
                                The selected fields must contain at least:
                                    AgLibraryFile.id_local (must be the first field),
                                    AgLibraryRootFolder.absolutePath,
                                    AgLibraryFolder.pathFromRoot,
                                    AgLibraryFile.baseName,
                                    AgLibraryFile.extension,
                                    AgLibraryFile.sidecarExtensions.
    -i, --ignore-case       ignore case on string comparison
        --skip-missing      skip missing source files

Miscellaneous:
    -V, --version           display version information and exit
        --help              display this help text and exit

Output control:
    -p, --pause             pause for key on exit
    -P, --no-pause          do not pause for key on exit
    -q, --quiet, --silent   suppress all normal output
    -s, --no-messages       suppress error messages
    -v, --verbose           verbose output
    -l, --log-file FILE     redirect output to FILE
        --log-append        append to log file

Only for mode list:
    -Z, --null              print 0 byte character after each file name

Only for mode copyfiles:
        --dry-run           perform a trial run with no changes made
        --move              move files instead of copy
        --overwrite         overwrite existing files in target
        --skip-existing     skip existing files in target
        --to DIRECTORY      target root directory

Only for mode copyexports:
        --dry-run           perform a trial run with no changes made
        --ext EXTENSION     change file extensions for source and target
        --include-copies    include exported copies such as "filename-*.jpg"
        --move              move files instead of copy
        --overwrite         overwrite existing files in target
        --skip-existing     skip existing files in target
        --skip-missing      skip missing files in source
        --source-dir DIRECTORY  source directory
        --to DIRECTORY      target root directory

Only for mode updatefiles:
        --dry-run           perform a trial run with no changes made (implies no-backup)
        --no-backup         do not backup LR catalog before write
        --to PATTERN        pattern for target update (multiple allowed)

    PATTERN:

    A pattern specifying what to update and how:

    Syntax:
        <PATTERN>         = <TARGET>=<SOURCES>
        <TARGET>          = description     updates description metadata
                          | filename        updates image file name
                          | title           updates title metadata
        <SOURCES>         = <one or more SOURCE>
        <SOURCE>          = <LITERAL> | {<REGEX-EXTRACTOR>}
        <LITERAL>         = <any text not containing {>
        <REGEX-EXTRACTOR> = <SEP><FIELD><SEP><REGEX><SEP><REPLACE-DEFS>
        <SEP>             = <any separator character>
        <FIELD>           = basename        file name without extension
                          | directory       file directory path (without root directory part)
                          | ext             file extension
                          | filename        full file name
                          | id              file database ID (AgLibraryFile.id_local)
                          | root-directory  file root directory
                          | sidecar-filename  full name or the file's sidecar file
        <REGEX>           = <any valid regular expression to extract values from FIELD>
        <REPLACE-DEFS>    = <one or more REPLACE-DEF>
        <REPLACE-DEF>     = <REGEX-GROUP>
                          | replace(search value)(old value)(new value)
                                in search value replace old value by new value
                          | replace-if(search value)(!old value A!old B!old C)(!new value A!new B!new C)
                                in search value replace old value by corresponding new value if it matches; ! may by any <SEP>
                          | sequence(starting value)(number of digits)
                                sequence to increase with every file
                          | sequence-distinct(value to group by)(starting value)(number of digits)(optional value to restart sequence)
                                sequence to increase with every group
                          | text(value)
                                returns the given value
                          | tolower(string)
                                converts a string to lowercase
                          | toupper(string)
                                converts a string to uppercase
        <REGEX-GROUP>     = \<DIGITS>       any group matched by <REGEX>
                                            any <REPLACE-DEF> parameter may contain one or more <REGEX-GROUP>

    Example:

        Rename all files in directory "2020.001 whatever".
        Assuming the files are named e.g. IMG_1234.jpg or IMG_2345a Title.jpg, the resulting filenames shall be:
            IMG_1234.jpg        --> 2020.001.001.jpg
            IMG_2345a Title.jpg --> 2020.001.002.B1.jpg
        while the last numeric part of the file name shall be a sequence grouped by the numeric part of the original filename.
        Furthermore the title part in the filename shall be written as the file's title metadata.

        lightroom-connect updatefiles ^
            --from myCatalog.lrcat ^
            --and directory like "%2020.001%/" ^
            --to "filename={!directory!(\d{4}\.\d{3})[^/]*/$!\1}.{!basename!^(\S{3}_\d{4})([a-z])?!\sequence-distinct(\1)(1)(3)\replace-if(\2)(!a!b!c!d)(!.B1!.B
2!.B3!.B4)}.{!ext!(.*)!\tolower(\1)}" ^
            --to "title={!basename!^\S{3}_\d{4}[a-z]?\W+(.*)$!\1}" ^

        lightroom-connect updatefiles       run lightroom-connect in updatefiles mode
        --from myCatalog.lrcat              use LR catalog "myCatalog.lrcat"
        --and directory like "%2020.001%/"  select files from directory matching "*2020.001*"
        --to "filename=                     update all selected filenames to:
            {!directory!(\d{4}\.\d{3})[^/]*/$!\1}
                                            start with the "2020.001" part of the directory name,
            .                               append a ".",
            {!basename!^(\S{3}_\d{4})([a-z])?!
                                            match "IMG_1234a" part of the filename to "\1\2",
                \sequence-distinct(\1)(1)(3)
                                            append a sequence distingished by the "IMG_1234" part
                                            (starting with 1 and padded by zeros to 3 digits),
                \replace-if(\2)(!a!b!c!d)(!.B1!.B2!.B3!.B4)
                                            append ".B1" if the 2nd part is "a"
                                            or append ".B2" if the 2nd part is "b"
                                            or append ".B3" if the 2nd part is "c"
                                            or append ".B4" if the 2nd part is "d"
            }
            .                               append a ".",
            {!ext!(.*)!\tolower(\1)}        append the lowercase file extension
        "
        --to "title=                        update the title of all selected files to:
            {!basename!^\S{3}_\d{4}[a-z]?\W+(.*)$!\1}
                                            the "Title" part in "IMG_1234a Title.jpg"
        "


CONDITION:

Any condition has the format: NAME OPERATOR VALUE

Valid NAMEs are:
    collection              image collection name
    camera-model            camera model name
    camera-sn               camera serial number
    city                    IPTC city
    collection              image collection
    copyright               image copyright text
    country                 IPTC country
    creator                 IPTC creator
    date                    image date formatted as "YYYY-MM-DD"
    directory               file source directory
    focal-length            focal length in mm
    image-rating            image rating in stars (VALUE is 0..5)
    iso                     ISO speed
    lens                    lens name
    location                IPTC location
    shutter-speed           shutter speed in seconds, e.g. "1/200"
    tag                     associated keyword

Valid OPERATORs are:
    eq, neq, gt, geq, lt, leq, isnull, isnotnull, like, =, !=, <>, >, >=, <, <=

ORDER-BY-FIELD:

Any field the filenames will be sorted by
Valid fields are:
    filename, creationtime


Exit status is 0 if any line is selected, 1 otherwise;
if any error occurs the exit status is 2.
````
