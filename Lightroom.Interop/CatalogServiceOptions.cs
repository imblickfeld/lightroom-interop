﻿using imBlickFeld.Lightroom.Loggers;

namespace imBlickFeld.Lightroom
{
    /// <summary>Options for <see cref="CatalogService"/>.</summary>
    public class CatalogServiceOptions
    {
        internal static readonly CatalogServiceOptions Default = new CatalogServiceOptions();

        /// <summary>Create a temporary copy of the catalog and do all work on this copy.</summary>
        public bool WorkOnCopy { get; set; }

        /// <summary>
        /// Optional file path of the working copy. This will only be used, if <see cref="WorkOnCopy"/> is <c>true</c>.
        /// If <c>null</c> a temporary file in temp dir will be created instead.
        /// This path must not point to the source catalog for it will be deleted after work.
        /// If the target file already exists it will be overwritten!
        /// </summary>
        public string WorkingCopyPath { get; set; }

        /// <summary>
        /// Hook to log debug output.
        /// </summary>
        public ILogger DebugLogger { get; set; }

        /// <summary>
        /// Hook to log trace output.
        /// </summary>
        public ILogger TraceLogger { get; set; }
    }
}
