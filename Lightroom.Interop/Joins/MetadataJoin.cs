﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Joins
{
    class MetadataJoin : Join
    {
        public MetadataJoin() {
            Table = "Adobe_AdditionalMetadata";
            Field = "image";
            RefTableField = "Adobe_images.id_local";
            SelectFields = new[] { "xmp" };
        }
    }
}
