﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace imBlickFeld.Lightroom.Joins
{
    [DebuggerDisplay("JOIN {Table} ON {Table}.{Field} = {RefTableField} SELECT {SelectFields}")]
    public class Join : IEquatable<Join>
    {
        public string Table { get; protected set; }
        public string Field { get; protected set; }
        public string RefTableField { get; protected set; }
        public string[] SelectFields { get; protected set; }

        public Join(string table, string field, string refTableField, string[] selectFields = null) {
            Table = table;
            Field = field;
            RefTableField = refTableField;
            SelectFields = selectFields;
        }

        public string ToJoin(JoinType type = JoinType.Inner) {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.Append("    ");
            switch (type) {
                case JoinType.LeftOuter:
                    sb.Append("LEFT ");  break;
                case JoinType.RightOuter:
                    sb.Append("RIGHT "); break;
                case JoinType.FullOuter:
                    sb.Append("FULL "); break;
                case JoinType.Cross:
                    sb.Append("CROSS "); break;
            }
            sb.Append("JOIN ").Append(Table);
            if (type != JoinType.Cross) {
                sb.Append(" ON ").Append(Table).Append(".").Append(Field)
                    .Append(" = ").Append(RefTableField);
            }
            return sb.ToString();
        }

        public string ToSelectFields() {
            return SelectFields == null ? "" : SelectFields.ToString(",\n    " + Table + ".");
        }

        public bool Equals(Join other) {
            return other != null
                && other.Table == Table
                && other.Field == Field
                && other.RefTableField == RefTableField;
        }

        public override bool Equals(object obj) {
            return Equals(obj as Join);
        }

        public override int GetHashCode() {
            return (Table == null ? 0 : Table.GetHashCode())
                ^ (Field == null ? 0 : Field.GetHashCode())
                ^ (RefTableField == null ? 0 : RefTableField.GetHashCode());
        }

        public enum JoinType
        {
            Inner, LeftOuter, RightOuter, FullOuter, Cross
        }
    }
}
