﻿using XmpCore;
using XmpCore.Options;

namespace imBlickFeld.Lightroom.Entities
{
    public class LRMetaData
    {
        public ulong ID { get; }
        public IXmpMeta Xmp { get; set; }

        public string Description {
            get { return Xmp.GetLocalizedText(XmpConstants.NsDC, "description", null, XmpConstants.XDefault)?.Value; }
            set { Xmp.SetLocalizedText(XmpConstants.NsDC, "description", null, XmpConstants.XDefault, value); }
        }

        public string Title {
            get { return Xmp.GetLocalizedText(XmpConstants.NsDC, "title", null, XmpConstants.XDefault)?.Value; }
            set { Xmp.SetLocalizedText(XmpConstants.NsDC, "title", null, XmpConstants.XDefault, value); }
        }


        public LRMetaData(ulong id, string sXmp) {
            ID = id;
            Xmp = XmpMetaFactory.ParseFromString(sXmp, new ParseOptions { DisallowDoctype = true });
        }

        /// <summary>Returns the serialized XMP packet.</summary>
        public override string ToString() {
            return XmpMetaFactory.SerializeToString(Xmp, new SerializeOptions {
                Indent = " ",
                OmitPacketWrapper = true
            });
        }
    }
}
