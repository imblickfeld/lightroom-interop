﻿using System.Diagnostics;

namespace imBlickFeld.Lightroom.Entities
{
    [DebuggerDisplay("{Directory,nq}{FileName,nq}")]
    public class LRFile
    {
        public ulong ID { get; set; }
        public string RootDirectory { get; set; }
        public string Directory { get; set; }
        public string FileName { get; set; }
        public string BaseName { get; set; }
        public string Extension { get; set; }
        public string SidecarFileName { get; set; }
    }
}
