﻿using imBlickFeld.Lightroom.Joins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Conditions
{
    public class Copyright : StringCondition
    {
        public Copyright(Operator op, string value) : base("AgLibraryIPTC.copyright", op, value) { }

        public override IEnumerable<Join> RequiredJoins => new[] {
            new Join("AgLibraryIPTC", "image", "Adobe_images.id_local"),
        };
    }
}
