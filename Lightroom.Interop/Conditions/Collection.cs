﻿using imBlickFeld.Lightroom.Joins;
using System.Collections.Generic;

namespace imBlickFeld.Lightroom.Conditions
{
    public class Collection : StringCondition
    {
        public Collection(Operator op, string value) : base("AgLibraryCollection.name", op, value) { }

        public override IEnumerable<Join> RequiredJoins => new[] {
            new Join("AgLibraryCollectionImage", "image", "Adobe_images.id_local"),
            new Join("AgLibraryCollection", "id_local", "AgLibraryCollectionImage.collection")
        };
    }
}
