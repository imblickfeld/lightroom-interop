﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Conditions
{
    public class ImageRating : NumericCondition
    {
        public ImageRating(Operator op, int value) : base("Adobe_images.rating", op, value) { }
    }
}
