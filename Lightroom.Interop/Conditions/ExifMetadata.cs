﻿using imBlickFeld.Lightroom.Joins;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Conditions
{
    public abstract class NumericExifMetadata : NumericCondition
    {
        protected NumericExifMetadata(string field, Operator op, int value) : base("AgHarvestedExifMetadata." + field, op, value) { }
        protected NumericExifMetadata(string field, Operator op, decimal value) : base("AgHarvestedExifMetadata." + field, op, value) { }
        protected NumericExifMetadata(string field, Operator op, double value) : base("AgHarvestedExifMetadata." + field, op, value) { }

        public override IEnumerable<Join> RequiredJoins => new[] {
            new Join("AgHarvestedExifMetadata", "image", "Adobe_images.id_local")
        };
    }

    public class IsoSpeedRating : NumericExifMetadata
    {
        public IsoSpeedRating(Operator op, int value) : base("isoSpeedRating", op, value) { }
    }

    public class FocalLength : NumericExifMetadata
    {
        public FocalLength(Operator op, int value) : base("focalLength", op, value) { }
    }

    public class ShutterSpeed : NumericExifMetadata
    {
        public ShutterSpeed(Operator op, decimal value) : base("shutterSpeed", op, value) { }
        public ShutterSpeed(Operator op, int value) : base("shutterSpeed", op, Math.Round(Math.Log(value, 2), 6)) { }

        public ShutterSpeed(Operator op, string value) : base("shutterSpeed", op, ParseValue(value)) { }

        private static double ParseValue(string value) {
            double d;
            if (!double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out d)) {
                var parts = value.Split(new[] { '/' }, 2);
                try {
                    double a = double.Parse(parts[0], CultureInfo.InvariantCulture);
                    double b = double.Parse(parts[1], CultureInfo.InvariantCulture);
                    d = b / a;
                } catch (FormatException) {
                    throw new ArgumentException("Please enter a valid shutter speed value in seconds such as \"1/200\". 2 seconds for example may be entered as \"2/1\".");
                }
            }
            return Math.Round(Math.Log(d, 2), 6);
        }
    }

    public class Date : StringCondition
    {
        public Date(Operator op, string value) : base("printf('%04d%02d%02d', AgHarvestedExifMetadata.dateYear, AgHarvestedExifMetadata.dateMonth, AgHarvestedExifMetadata.dateDay)", op, DateTime.Parse(value).ToString("yyyyMMdd")) { }

        public override IEnumerable<Join> RequiredJoins => new[] {
            new Join("AgHarvestedExifMetadata", "image", "Adobe_images.id_local")
        };
    }

    public class CameraModel : StringCondition
    {
        public CameraModel(Operator op, string value) : base("AgInternedExifCameraModel.value", op, value) { }

        public override IEnumerable<Join> RequiredJoins => new[] {
            new Join("AgHarvestedExifMetadata", "image", "Adobe_images.id_local"),
            new Join("AgInternedExifCameraModel", "id_local", "AgHarvestedExifMetadata.cameraModelRef")
        };
    }

    public class CameraSN : StringCondition
    {
        public CameraSN(Operator op, string value) : base("AgInternedExifCameraSN.value", op, value) { }

        public override IEnumerable<Join> RequiredJoins => new[] {
            new Join("AgHarvestedExifMetadata", "image", "Adobe_images.id_local"),
            new Join("AgInternedExifCameraSN", "id_local", "AgHarvestedExifMetadata.cameraSNRef")
        };
    }

    public class Lens : StringCondition
    {
        public Lens(Operator op, string value) : base("AgInternedExifLens.value", op, value) { }

        public override IEnumerable<Join> RequiredJoins => new[] {
            new Join("AgHarvestedExifMetadata", "image", "Adobe_images.id_local"),
            new Join("AgInternedExifLens", "id_local", "AgHarvestedExifMetadata.lensRef")
        };
    }
}
