﻿using imBlickFeld.Lightroom.Joins;
using System.Collections.Generic;

namespace imBlickFeld.Lightroom.Conditions
{
    public class Tag : StringCondition
    {
        public Tag(Operator op, string value) : base("AgLibraryKeyword.name", op, value) { }

        public override IEnumerable<Join> RequiredJoins => new[] {
            new Join("AgLibraryKeywordImage", "image", "Adobe_images.id_local"),
            new Join("AgLibraryKeyword", "id_local", "AgLibraryKeywordImage.tag")
        };
    }
}
