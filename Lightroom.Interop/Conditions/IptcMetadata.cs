﻿using imBlickFeld.Lightroom.Joins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Conditions
{
    public class StringIptcMetadata : StringCondition
    {
        private readonly IEnumerable<Join> _requiredJoins;

        protected StringIptcMetadata(string table, string iptcMetadataRefField, Operator op, string value) : base(table + ".value", op, value) {
            _requiredJoins = new[] {
                new Join("AgHarvestedIptcMetadata", "image", "Adobe_images.id_local"),
                new Join(table, "id_local", "AgHarvestedIptcMetadata." + iptcMetadataRefField)
            };
        }

        public override IEnumerable<Join> RequiredJoins => _requiredJoins;
    }

    public class City : StringIptcMetadata
    {
        public City(Operator op, string value) : base("AgInternedIptcCity", "cityRef", op, value) { }
    }

    public class Country : StringIptcMetadata
    {
        public Country(Operator op, string value) : base("AgInternedIptcCountry", "countryRef", op, value) { }
    }

    public class Creator : StringIptcMetadata
    {
        public Creator(Operator op, string value) : base("AgInternedIptcCreator", "creatorRef", op, value) { }
    }

    public class Location : StringIptcMetadata
    {
        public Location(Operator op, string value) : base("AgInternedIptcLocation", "locationRef", op, value) { }
    }
}
