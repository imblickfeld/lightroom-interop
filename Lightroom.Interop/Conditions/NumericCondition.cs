﻿using System;
using System.Globalization;

namespace imBlickFeld.Lightroom.Conditions
{
    public class NumericCondition : Condition
    {
        public NumericCondition(string name, Operator op, byte value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, sbyte value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, ushort value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, uint value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, ulong value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, short value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, int value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, long value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, decimal value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, double value) : base(name, op, value) { }
        public NumericCondition(string name, Operator op, float value) : base(name, op, value) { }

        public override object Value {
            get => base.Value;
            set {
                if (!(value is byte || value is sbyte || value is ushort || value is uint || value is ulong || value is short || value is int || value is long || value is decimal || value is double || value is float))
                    throw new ArgumentException("Value must be a numeric value!");
                base.Value = value;
            }
        }

        public override string ValueToSql() {
            switch (Type.GetTypeCode(Value.GetType())) {
                case TypeCode.Single:
                    return ((float)Value).ToString(CultureInfo.InvariantCulture);
                case TypeCode.Double:
                    return ((double)Value).ToString(CultureInfo.InvariantCulture);
                case TypeCode.Decimal:
                    return ((decimal)Value).ToString(CultureInfo.InvariantCulture);
                default:
                    return Value.ToString();
            }
        }
    }
}
