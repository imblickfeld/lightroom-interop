﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Conditions
{
    public class Directory : StringCondition
    {
        public Directory(Operator op, string value) : base("AgLibraryFolder.pathFromRoot", op, value) { }
    }
}
