﻿using imBlickFeld.Lightroom.Joins;
using System;
using System.Collections.Generic;
using System.Linq;

namespace imBlickFeld.Lightroom.Conditions
{
    public abstract class Condition
    {
        public string Name { get; protected set; }
        public Operator Op { get; protected set; }
        public virtual object Value { get; set; }
        public virtual IEnumerable<Join> RequiredJoins => null;

        protected Condition(string name, Operator op, object value) {
            Name = name;
            Op = op;
            Value = value;
        }

        public KeyValuePair<NextConditionType, Condition>? Next { get; private set; }

        public Condition Add(NextConditionType type, Condition condition) {
            if (type == NextConditionType.None)
                throw new ArgumentException("Type may only be None for the first condition, but this is not the first condition.");
            if (condition != null)
                Next = new KeyValuePair<NextConditionType, Condition>(type, condition);
            return this;
        }

        public override string ToString() {
            return ToString(false);
        }

        public string ToString(bool ignoreCase) {
            var op = Value == null ? Operator.IsNull : Op;
            return "( " + Name + " "
                + op.ToSql()
                + (op == Operator.IsNull || op == Operator.IsNotNull ? "" : " " + ValueToSql())
                + (ignoreCase ? " COLLATE NOCASE" : "")
                + ")";
        }

        public virtual string ValueToSql() {
            return Value.ToString();
        }

        public IEnumerable<KeyValuePair<NextConditionType, Condition>> Enumerate() {
            KeyValuePair<NextConditionType, Condition>? c = new KeyValuePair<NextConditionType, Condition>(NextConditionType.None, this);
            while (c != null) {
                var cc = c.Value;
                yield return cc;
                c = cc.Value.Next;
            }
        }

        public string ToWhere(bool ignoreCase = false) {
            return " WHERE " + Enumerate()
                .Select(x => x.Key.ToSql() + " " + x.Value.ToString(ignoreCase))
                .ToString("\n");
        }

        public IEnumerable<Join> ToRequiredJoins() {
            return Enumerate()
                .Where(x => x.Value.RequiredJoins != null)
                .SelectMany(x => x.Value.RequiredJoins);
        }
    }

    public enum NextConditionType
    {
        None,
        And,
        Or,
    }

    public enum Operator
    {
        /// <summary>Is equal to, =</summary>
        Eq,
        /// <summary>Is not equal to, !=</summary>
        Neq,
        /// <summary>Is greater than, &gt;</summary>
        Gt,
        /// <summary>Is greater than or equal to, &gt;=</summary>
        Geq,
        /// <summary>Is lower than, &lt;</summary>
        Lt,
        /// <summary>Is lower than or equal to, &lt;=</summary>
        Leq,
        /// <summary>IS NULL</summary>
        IsNull,
        /// <summary>IS NOT NULL</summary>
        IsNotNull,
        /// <summary>LIKE</summary>
        Like
    }

    static class ConditionExtensions
    {
        public static string ToSql(this NextConditionType type) {
            switch (type) {
                case NextConditionType.None:
                    return "";
                case NextConditionType.And:
                    return "AND";
                case NextConditionType.Or:
                    return "OR";
                default:
                    throw new ArgumentException("Unknown value: " + type);
            }
        }

        public static string ToSql(this Operator op) {
            switch (op) {
                case Operator.Eq:
                    return "=";
                case Operator.Neq:
                    return "<>";
                case Operator.Gt:
                    return ">";
                case Operator.Geq:
                    return ">=";
                case Operator.Lt:
                    return "<";
                case Operator.Leq:
                    return "<=";
                case Operator.IsNull:
                    return "IS NULL";
                case Operator.IsNotNull:
                    return "IS NOT NULL";
                case Operator.Like:
                    return "LIKE";
                default:
                    throw new ArgumentException("Unknown value: " + op);
            }
        }
    }
}
