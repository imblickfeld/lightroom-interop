﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.Conditions
{
    public abstract class StringCondition : Condition
    {
        protected StringCondition(string name, Operator op, string value) : base(name, op, value) { }

        public override string ValueToSql() {
            return "'" + Value.ToString().Replace("'", "''") + "'";
        }
    }
}
