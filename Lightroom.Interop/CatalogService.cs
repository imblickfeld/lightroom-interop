﻿using imBlickFeld.Lightroom.Conditions;
using imBlickFeld.Lightroom.DB;
using imBlickFeld.Lightroom.Entities;
using imBlickFeld.Lightroom.Joins;
using imBlickFeld.Lightroom.OrderBys;
using imBlickFeld.Lightroom.Properties;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;

namespace imBlickFeld.Lightroom
{
    public partial class CatalogService : IDisposable
    {
        /// <summary>Gets the LR catalog to query.</summary>
        public string LRCatFilename { get; }

        public CatalogServiceOptions Options { get; }

        SQLiteConnection _connection = null;
        public SQLiteConnection Connection {
            get {
                if (_connection != null)
                    return _connection;
                _connection = SqlHelper.GetConnection(LRCatFilename);
                try {
                    LogDebug("Open connection to: " + _connection.ConnectionString);
                    _connection.Open();
                } catch (SQLiteException) {
                    throw new CatalogException(Resources.FailedToOpenCatalog);
                }
                return _connection;
            }
        }

        /// <summary>Creates a new instance of <see cref="CatalogService"/>.</summary>
        /// <param name="lrcatFilename">the LR catalog to query.</param>
        public CatalogService(string lrcatFilename, CatalogServiceOptions options = null) {
            LRCatFilename = lrcatFilename;
            Options = options ?? CatalogServiceOptions.Default;

            if (Options.WorkOnCopy) {
                var tmpCat = Options.WorkingCopyPath ?? Path.GetTempFileName();
                LogDebug("Working on catalog copy: " + tmpCat);
                if (Path.GetFullPath(tmpCat) == Path.GetFullPath(lrcatFilename))
                    throw new CatalogException("The Lightroom catalog working copy must be different from the original catalog.");
                File.Copy(lrcatFilename, tmpCat, true);
                LRCatFilename = tmpCat;
            }
        }

        #region Query/UpdateFileNames SQL statement

        private const string GET_FILES =
            @"PRAGMA case_sensitive_like = {2};
SELECT DISTINCT 
    AgLibraryFile.id_local, 
    AgLibraryRootFolder.absolutePath, 
    AgLibraryFolder.pathFromRoot, 
    AgLibraryFile.baseName, 
    AgLibraryFile.extension, 
    AgLibraryFile.sidecarExtensions{0}
FROM
    AgLibraryFile
    JOIN AgLibraryFolder ON AgLibraryFolder.id_local = AgLibraryFile.folder
    JOIN AgLibraryRootFolder ON AgLibraryRootFolder.id_local = AgLibraryFolder.rootFolder
    JOIN Adobe_images ON Adobe_images.rootFile = AgLibraryFile.id_local{1}
";

        private const string UPDATE_FILENAME = @"UPDATE AgLibraryFile
SET basename = @basename, extension = @extension, idx_filename = @idx_filename, lc_idx_filename = @lc_idx_filename, lc_idx_filenameExtension = @lc_idx_filenameExtension
WHERE id_local = @id_local";

        #endregion

        /// <summary>
        /// Returns all files from the LR catalog filtered by the given filter <paramref name="conditions"/>.
        /// </summary>
        public IEnumerable<LRFile> QueryFileNames(Condition conditions = null, ICollection<Join> joins = null, ICollection<OrderByField> orderBy = null, bool ignoreCase = false) {
            var con = Connection;
            using (var cmd = new SQLiteCommand(con)) {
                var additionalJoins = joins ?? Enumerable.Empty<Join>();
                if (conditions != null)
                    additionalJoins = additionalJoins.Union(conditions.ToRequiredJoins());
                cmd.CommandText = string.Format(GET_FILES,
                    joins == null ? "" : joins.Select(x => x.ToSelectFields()).ToString(),
                    additionalJoins.Distinct().Select(x => x.ToJoin()).ToString(""),
                    ignoreCase ? "false" : "true")
                    + (conditions == null ? "" : conditions.ToWhere(ignoreCase))
                    + orderBy.ToSql();
                LogDebug("Querying file names...");
                LogTrace(cmd.CommandText);
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read()) {
                        string rootDir = reader["absolutePath"] as string ?? "";
                        string dir = reader["pathFromRoot"] as string;
                        yield return new LRFile {
                            ID = (ulong)reader.GetInt64(0),
                            RootDirectory = rootDir,
                            Directory = dir,
                            FileName = reader["baseName"] + "." + reader["extension"],
                            BaseName = reader["baseName"] as string,
                            Extension = reader["extension"] as string,
                            SidecarFileName = (reader["sidecarExtensions"] as string) == "" ? null : reader["baseName"] + "." + reader["sidecarExtensions"]
                        };
                    }
            }
        }

        /// <summary>
        /// Returns all files from the LR catalog specified by the given <paramref name="sql"/> query. The selected fields must contain at least:
        /// <c>AgLibraryFile.id_local</c> as the first field, <c>AgLibraryRootFolder.absolutePath</c>, <c>AgLibraryFolder.pathFromRoot</c>, <c>AgLibraryFile.baseName</c>, <c>AgLibraryFile.extension</c>
        /// and <c>AgLibraryFile.sidecarExtensions</c>.
        /// </summary>
        public IEnumerable<LRFile> QueryFileNames(string sql) {
            var con = Connection;
            using (var cmd = new SQLiteCommand(con)) {
                cmd.CommandText = sql;
                LogDebug("Querying file names...");
                LogTrace(cmd.CommandText);
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read()) {
                        string rootDir = reader["absolutePath"] as string ?? "";
                        string dir = reader["pathFromRoot"] as string;
                        yield return new LRFile {
                            ID = (ulong)reader.GetInt64(0),
                            RootDirectory = rootDir,
                            Directory = dir,
                            FileName = reader["baseName"] + "." + reader["extension"],
                            BaseName = reader["baseName"] as string,
                            Extension = reader["extension"] as string,
                            SidecarFileName = (reader["sidecarExtensions"] as string) == "" ? null : reader["baseName"] + "." + reader["sidecarExtensions"]
                        };
                    }
            }
        }

        /// <summary>
        /// Updates the filename in <c>AgLibraryFile</c>.
        /// </summary>
        /// <param name="id">The primary key (<c>id_local</c>).</param>
        /// <param name="baseName">The new filename without extension.</param>
        /// <param name="extension">The new file extension.</param>
        /// <returns>The number of affected rows, should be 1 if the file was updated or 0 if no dataset with the ID exists.</returns>
        public int UpdateFileName(ulong id, string baseName, string extension) {
            var con = Connection;
            using (var cmd = new SQLiteCommand(con)) {
                cmd.CommandText = UPDATE_FILENAME;
                string fn = baseName + "." + extension;
                string lcfn = fn.ToLowerInvariant();
                string lcext = extension.ToLowerInvariant();
                var p = cmd.Parameters;
                p.AddWithValue("@basename", baseName);
                p.AddWithValue("@extension", extension);
                p.AddWithValue("@idx_filename", fn);
                p.AddWithValue("@lc_idx_filename", lcfn);
                p.AddWithValue("@lc_idx_filenameExtension", lcext);
                p.AddWithValue("@id_local", id);
                LogDebug("Updating file name...");
                LogTrace(() => cmd.CommandText
                                    .Replace("@basename", "'" + baseName + "'")
                                    .Replace("@extension", "'" + extension + "'")
                                    .Replace("@idx_filename", "'" + fn + "'")
                                    .Replace("@lc_idx_filenameExtension", "'" + lcext + "'")
                                    .Replace("@lc_idx_filename", "'" + lcfn + "'")
                                    .Replace("@id_local", id.ToString()));
                return cmd.ExecuteNonQuery();
            }
        }

        #region QueryMetadataForFiles SQL statement

        private const string GET_METADATA =
            @"SELECT
    AgLibraryFile.id_local,
    Adobe_AdditionalMetadata.id_local as xmp_id_local,
    Adobe_AdditionalMetadata.xmp
FROM
    AgLibraryFile
    JOIN AgLibraryFolder ON AgLibraryFolder.id_local = AgLibraryFile.folder
    JOIN Adobe_images ON Adobe_images.rootFile = AgLibraryFile.id_local
    JOIN Adobe_AdditionalMetadata ON Adobe_AdditionalMetadata.image = Adobe_images.id_local
";
        // --JOIN AgLibraryRootFolder ON AgLibraryRootFolder.id_local = AgLibraryFolder.rootFolder

        private const string UPDATE_METADATA =
            @"UPDATE Adobe_AdditionalMetadata SET xmp = @xmp WHERE id_local = @id_local";

        #endregion

        public IDictionary<ulong, IList<LRMetaData>> QueryMetadataForFiles(Condition conditions = null) {
            var dic = new Dictionary<ulong, IList<LRMetaData>>();
            var con = Connection;
            using (var cmd = new SQLiteCommand(con)) {
                cmd.CommandText = GET_METADATA
                    + (conditions == null ? "" : conditions.ToWhere());
                LogDebug("Querying XMP metadata...");
                LogTrace(cmd.CommandText);
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read()) {
                        ulong id = (ulong)reader.GetInt64(0);
                        ulong idXmp = (ulong)reader.GetInt64(1);
                        var metadata = new LRMetaData(idXmp, reader["xmp"] as string);
                        if (!dic.TryGetValue(id, out var list)) {
                            list = new List<LRMetaData>();
                            dic.Add(id, list);
                        }
                        list.Add(metadata);
                    }
            }
            return dic;
        }

        /// <summary>
        /// Updates the XMP string in <c>Adobe_AdditionalMetadata</c>.
        /// </summary>
        /// <param name="id">The primary key (<c>id_local</c>).</param>
        /// <param name="xmp">The serialized XMP.</param>
        /// <returns>The number of affected rows, should be 1 if the file was updated or 0 if no dataset with the ID exists.</returns>
        public int UpdateMetadata(ulong id, string xmp) {
            var con = Connection;
            using (var cmd = new SQLiteCommand(con)) {
                cmd.CommandText = UPDATE_METADATA;
                var p = cmd.Parameters;
                p.AddWithValue("@id_local", id);
                p.AddWithValue("@xmp", xmp);
                LogDebug("Updating XMP metadata...");
                LogTrace(() => cmd.CommandText
                                    .Replace("@id_local", id.ToString())
                                    .Replace("@xmp", "'" + xmp.Replace("\n", "") + "...'"));
                return cmd.ExecuteNonQuery();
            }
        }


        private void LogDebug(string msg) {
            if (Options.DebugLogger != null)
                Options.DebugLogger.Log(msg);
        }

        private void LogTrace(string msg) {
            if (Options.TraceLogger != null)
                Options.TraceLogger.Log(msg);
        }

        private void LogTrace(Func<string> msg) {
            if (Options.TraceLogger != null)
                Options.TraceLogger.Log(msg());
        }

        #region IDisposable Members

        public void Dispose() {
            if (_connection != null)
                Connection.Dispose();
            if (Options.WorkOnCopy) {
                LogDebug("Deleting catalog copy: " + LRCatFilename);
                File.Delete(LRCatFilename);
            }
        }

        #endregion
    }
}
