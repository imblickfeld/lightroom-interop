﻿using System.Data.SQLite;

namespace imBlickFeld.Lightroom.DB
{
    internal static class SqlHelper
    {
        public static SQLiteConnection GetConnection(string dbFileName) {
            return new SQLiteConnection("Data Source=" + dbFileName);
        }
    }
}
