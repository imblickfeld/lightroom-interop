﻿namespace imBlickFeld.Lightroom.Loggers
{
    public interface ILogger
    {
        void Log(string msg);
    }
}
