﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom.OrderBys
{
    public class OrderByField
    {
        public readonly string TableWithField;

        internal OrderByField(string tableWithField) {
            TableWithField = tableWithField;
        }
    }

    public enum OrderBy
    {
        Filename,
        CreationTime
    }

    public static class OrderByExtensions
    {
        public static OrderByField ToOrderByField(this OrderBy e) {
            switch (e) {
                case OrderBy.Filename:
                    return new OrderByField("AgLibraryRootFolder.absolutePath, AgLibraryFolder.pathFromRoot, AgLibraryFile.baseName, AgLibraryFile.extension");
                case OrderBy.CreationTime:
                    return new OrderByField("Adobe_images.captureTime");
                default:
                    throw new ArgumentException("invalid OrderBy value: " + e);
            }
        }

        public static string ToSql(this ICollection<OrderByField> e) {
            if (e == null || e.Count == 0)
                return "";
            return " ORDER BY " + e.Select(x => x.TableWithField).ToString(", ");
        }
    }
}
