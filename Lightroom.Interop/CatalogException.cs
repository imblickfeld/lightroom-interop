﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.Lightroom
{
    public class CatalogException : Exception
    {
        public CatalogException() { }

        public CatalogException(string message) : base(message) { }

        public CatalogException(string message, Exception innerException) : base(message, innerException) { }

        protected CatalogException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
