PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

-- TABLES

CREATE TABLE AgLibraryRootFolder (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    absolutePath UNIQUE NOT NULL DEFAULT '',
    name NOT NULL DEFAULT '',
    relativePathFromCatalog
);
CREATE TABLE "AgLibraryFolder" (
	"id_local"	INTEGER,
	"id_global"	 NOT NULL UNIQUE,
	"pathFromRoot"	 NOT NULL DEFAULT '',
	"rootFolder"	INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY("rootFolder") REFERENCES "AgLibraryRootFolder"("id_local"),
	PRIMARY KEY("id_local")
);

CREATE TABLE Adobe_namedIdentityPlate (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    description,
    identityPlate,
    identityPlateHash,
    moduleFont,
    moduleSelectedTextColor,
    moduleTextColor
);
CREATE TABLE Adobe_variables (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    name,
    value
);
CREATE TABLE Adobe_variablesTable (
	    id_local INTEGER PRIMARY KEY,
	    id_global UNIQUE NOT NULL,
	    name,
	    type,
	    value NOT NULL DEFAULT ''
	);
CREATE TABLE AgDeletedOzAlbumAssetIds(
	ozCatalogId NOT NULL,
	ozAlbumAssetId NOT NULL,
	changeCounter DEFAULT 0
);
CREATE TABLE AgDeletedOzAlbumIds(
	ozCatalogId NOT NULL,
	ozAlbumId NOT NULL,
	changeCounter DEFAULT 0
);
CREATE TABLE AgDeletedOzAssetIds(
	ozCatalogId NOT NULL,
	ozAssetId NOT NULL,
	changeCounter DEFAULT 0
);
CREATE TABLE AgHarvestedMetadataWorklist (
    id_local INTEGER PRIMARY KEY,
    taskID UNIQUE NOT NULL DEFAULT '',
    taskStatus NOT NULL DEFAULT 'pending',
    whenPosted NOT NULL DEFAULT ''
);
CREATE TABLE AgInternedExifCameraModel (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedExifCameraSN (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedExifLens (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedIptcCity (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedIptcCountry (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedIptcCreator (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedIptcIsoCountryCode (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedIptcJobIdentifier (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedIptcLocation (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgInternedIptcState (
    id_local INTEGER PRIMARY KEY,
    searchIndex,
    value
);
CREATE TABLE AgLastCatalogExport ( image INTEGER PRIMARY KEY );
CREATE TABLE AgLibraryCollection (
    id_local INTEGER PRIMARY KEY,
    creationId NOT NULL DEFAULT '',
    genealogy NOT NULL DEFAULT '',
    imageCount,
    name NOT NULL DEFAULT '',
    parent INTEGER,
    systemOnly NOT NULL DEFAULT '',
	FOREIGN KEY("parent") REFERENCES "AgLibraryCollection"("id_local")
);
CREATE TABLE AgLibraryCollectionChangeCounter(
	collection PRIMARY KEY,
	changeCounter DEFAULT 0
);
CREATE TABLE AgLibraryCollectionContent (
    id_local INTEGER PRIMARY KEY,
    collection INTEGER NOT NULL DEFAULT 0,
    content,
    owningModule,
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local")
);
CREATE TABLE AgLibraryCollectionCoverImage(
	collection PRIMARY KEY,
	collectionImage NOT NULL
);
CREATE TABLE AgLibraryCollectionImage (
    id_local INTEGER PRIMARY KEY,
    collection INTEGER NOT NULL DEFAULT 0,
    image INTEGER NOT NULL DEFAULT 0,
    pick NOT NULL DEFAULT 0,
    positionInCollection,
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local"),
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryCollectionImageChangeCounter(
	collectionImage PRIMARY KEY,
	collection NOT NULL,
	image NOT NULL,
	changeCounter DEFAULT 0,
	FOREIGN KEY("collectionImage") REFERENCES "AgLibraryCollectionImage"("id_local"),
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local"),
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryCollectionImageOzAlbumAssetIds(
	collectionImage NOT NULL,
	collection NOT NULL,
	image NOT NULL,
	ozCatalogId NOT NULL, -- TODO FK?
	ozAlbumAssetId DEFAULT "pending", -- TODO FK?
	FOREIGN KEY("collectionImage") REFERENCES "AgLibraryCollectionImage"("id_local"),
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local"),
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryCollectionImageOzSortOrder(
	collectionImage  PRIMARY KEY,
	collection NOT NULL,
	positionInCollection NOT NULL,
	ozSortOrder NOT NULL,
	FOREIGN KEY("collectionImage") REFERENCES "AgLibraryCollectionImage"("id_local"),
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local")
);
CREATE TABLE AgLibraryCollectionOzAlbumIds(
	collection NOT NULL,
	ozCatalogId NOT NULL,
	ozAlbumId DEFAULT "pending",
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local")
);
CREATE TABLE AgLibraryCollectionStack (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    collapsed INTEGER NOT NULL DEFAULT 0,
    collection INTEGER NOT NULL DEFAULT 0,
    text NOT NULL DEFAULT '',
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local")
);
CREATE TABLE AgLibraryCollectionStackData(
    stack INTEGER,
    collection INTEGER NOT NULL DEFAULT 0,
    stackCount INTEGER NOT NULL DEFAULT 0,
    stackParent INTEGER,
	FOREIGN KEY("stack") REFERENCES "AgLibraryCollectionStack"("id_local"),
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local")
);
CREATE TABLE AgLibraryCollectionStackImage (
    id_local INTEGER PRIMARY KEY,
    collapsed INTEGER NOT NULL DEFAULT 0,
    collection INTEGER NOT NULL DEFAULT 0,
    image INTEGER NOT NULL DEFAULT 0,
    position NOT NULL DEFAULT '',
    stack INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local"),
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local"),
	FOREIGN KEY("stack") REFERENCES "AgLibraryCollectionStack"("id_local")
);
CREATE TABLE AgLibraryCollectionSyncedAlbumData(
	collection NOT NULL,
	payloadKey NOT NULL,
	payloadData NOT NULL,
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local")
);
CREATE TABLE AgLibraryCollectionTrackedAssets (
	collection NOT NULL,
	ozCatalogId DEFAULT "current",
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local")
);
CREATE TABLE AgLibraryFace (
    id_local INTEGER PRIMARY KEY,
    bl_x,
    bl_y,
    br_x,
    br_y,
    cluster INTEGER,
    compatibleVersion,
    image INTEGER NOT NULL DEFAULT 0,
    imageOrientation NOT NULL DEFAULT '',
    orientation,
    origination NOT NULL DEFAULT 0,
    propertiesCache,
    regionType NOT NULL DEFAULT 0,
    tl_x NOT NULL DEFAULT '',
    tl_y NOT NULL DEFAULT '',
    touchCount NOT NULL DEFAULT 0,
    touchTime NOT NULL DEFAULT -63113817600,
    tr_x,
    tr_y,
    version,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryFaceCluster (
    id_local INTEGER PRIMARY KEY,
    keyFace INTEGER
);
CREATE TABLE AgLibraryFaceData (
    id_local INTEGER PRIMARY KEY,
    data,
    face INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY("face") REFERENCES "AgLibraryFace"("id_local")
);
CREATE TABLE AgLibraryFileAssetMetadata(
	fileId PRIMARY KEY,
	sha256 NOT NULL,
	fileSize DEFAULT 0
);
CREATE TABLE AgLibraryFolderStack (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    collapsed INTEGER NOT NULL DEFAULT 0,
    text NOT NULL DEFAULT ''
);
CREATE TABLE AgLibraryFolderStackData (
    stack INTEGER,
    stackCount INTEGER NOT NULL DEFAULT 0,
    stackParent INTEGER,
	FOREIGN KEY("stack") REFERENCES "AgLibraryFolderStack"("id_local"),
	FOREIGN KEY("stackParent") REFERENCES "AgLibraryFolderStack"("id_local")
);
CREATE TABLE AgLibraryFolderStackImage (
    id_local INTEGER PRIMARY KEY,
    collapsed INTEGER NOT NULL DEFAULT 0,
    image INTEGER NOT NULL DEFAULT 0,
    position NOT NULL DEFAULT '',
    stack INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local"),
	FOREIGN KEY("stack") REFERENCES "AgLibraryFolderStack"("id_local")
);
CREATE TABLE AgLibraryIPTC (
    id_local INTEGER PRIMARY KEY,
    caption,
    copyright,
    image INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryImageChangeCounter(
	image PRIMARY KEY,
	changeCounter DEFAULT 0,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryImageOzAssetIds(
	image NOT NULL,
	ozCatalogId NOT NULL,
	ozAssetId DEFAULT "pending",
	ownedByCatalog DEFAULT 'Y',
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryImageSyncedAssetData (
	image NOT NULL,
	payloadKey NOT NULL,
	payloadData NOT NULL,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryImageXMPUpdater (
    id_local INTEGER PRIMARY KEY,
    taskID UNIQUE NOT NULL DEFAULT '',
    taskStatus NOT NULL DEFAULT 'pending',
    whenPosted NOT NULL DEFAULT ''
);
CREATE TABLE AgLibraryImport (
    id_local INTEGER PRIMARY KEY,
    imageCount,
    importDate NOT NULL DEFAULT '',
    name
);
CREATE TABLE AgLibraryImportImage (
    id_local INTEGER PRIMARY KEY,
    image INTEGER NOT NULL DEFAULT 0,
    import INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryKeyword (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    dateCreated NOT NULL DEFAULT '',
    genealogy NOT NULL DEFAULT '',
    imageCountCache DEFAULT -1,
    includeOnExport INTEGER NOT NULL DEFAULT 1,
    includeParents INTEGER NOT NULL DEFAULT 1,
    includeSynonyms INTEGER NOT NULL DEFAULT 1,
    keywordType,
    lastApplied,
    lc_name,
    name,
    parent INTEGER,
	FOREIGN KEY("parent") REFERENCES "AgLibraryKeyword"("id_local")
);
CREATE TABLE AgLibraryKeywordCooccurrence (
    id_local INTEGER PRIMARY KEY,
    tag1 NOT NULL DEFAULT '',
    tag2 NOT NULL DEFAULT '',
    value NOT NULL DEFAULT 0,
	FOREIGN KEY("tag1") REFERENCES "AgLibraryKeyword"("id_local"),
	FOREIGN KEY("tag2") REFERENCES "AgLibraryKeyword"("id_local")
);
CREATE TABLE AgLibraryKeywordFace (
    id_local INTEGER PRIMARY KEY,
    face INTEGER NOT NULL DEFAULT 0,
    keyFace INTEGER,
    rankOrder,
    tag INTEGER NOT NULL DEFAULT 0,
    userPick INTEGER,
    userReject INTEGER,
	FOREIGN KEY("face") REFERENCES "AgLibraryFace"("id_local"),
	FOREIGN KEY("tag") REFERENCES "AgLibraryKeyword"("id_local")
);
CREATE TABLE AgLibraryKeywordImage (
    id_local INTEGER PRIMARY KEY,
    image INTEGER NOT NULL DEFAULT 0,
    tag INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local"),
	FOREIGN KEY("tag") REFERENCES "AgLibraryKeyword"("id_local")
);
CREATE TABLE AgLibraryKeywordPopularity (
    id_local INTEGER PRIMARY KEY,
    occurrences NOT NULL DEFAULT 0,
    popularity NOT NULL DEFAULT 0,
    tag UNIQUE NOT NULL DEFAULT '',
	FOREIGN KEY("tag") REFERENCES "AgLibraryKeyword"("id_local")
);
CREATE TABLE AgLibraryKeywordSynonym (
    id_local INTEGER PRIMARY KEY,
    keyword INTEGER NOT NULL DEFAULT 0,
    lc_name,
    name,
	FOREIGN KEY("keyword") REFERENCES "AgLibraryKeyword"("id_local")
);
CREATE TABLE AgLibraryOzFeedbackInfo (
    id_local INTEGER PRIMARY KEY,
    image NOT NULL DEFAULT '',
    lastFeedbackTime,
    lastReadTime,
    newCommentCount NOT NULL DEFAULT 0,
    newFavoriteCount NOT NULL DEFAULT 0,
    ozAssetId NOT NULL DEFAULT '',
    ozCatalogId NOT NULL DEFAULT '',
    ozSpaceId NOT NULL DEFAULT '',
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryPublishedCollection (
    id_local INTEGER PRIMARY KEY,
    creationId NOT NULL DEFAULT '',
    genealogy NOT NULL DEFAULT '',
    imageCount,
    isDefaultCollection,
    name NOT NULL DEFAULT '',
    parent INTEGER,
    publishedUrl,
    remoteCollectionId,
    systemOnly NOT NULL DEFAULT '',
	FOREIGN KEY("parent") REFERENCES "AgLibraryPublishedCollection"("id_local")
);
CREATE TABLE AgLibraryPublishedCollectionContent (
    id_local INTEGER PRIMARY KEY,
    collection INTEGER NOT NULL DEFAULT 0,
    content,
    owningModule,
	FOREIGN KEY("collection") REFERENCES "AgLibraryPublishedCollection"("id_local")
);
CREATE TABLE AgLibraryPublishedCollectionImage (
    id_local INTEGER PRIMARY KEY,
    collection INTEGER NOT NULL DEFAULT 0,
    image INTEGER NOT NULL DEFAULT 0,
    pick NOT NULL DEFAULT 0,
    positionInCollection,
	FOREIGN KEY("collection") REFERENCES "AgLibraryPublishedCollection"("id_local"),
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgLibraryUpdatedImages (
	image INTEGER PRIMARY KEY,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgMRULists (
    id_local INTEGER PRIMARY KEY,
    listID NOT NULL DEFAULT '',
    timestamp NOT NULL DEFAULT 0,
    value NOT NULL DEFAULT ''
);
CREATE TABLE AgMetadataSearchIndex (
    id_local INTEGER PRIMARY KEY,
    exifSearchIndex NOT NULL DEFAULT '',
    image INTEGER,
    iptcSearchIndex NOT NULL DEFAULT '',
    otherSearchIndex NOT NULL DEFAULT '',
    searchIndex NOT NULL DEFAULT '',
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgOutputImageAsset (
    id_local INTEGER PRIMARY KEY,
    assetId NOT NULL DEFAULT '',
    collection INTEGER NOT NULL DEFAULT 0,
    image INTEGER NOT NULL DEFAULT 0,
    moduleId NOT NULL DEFAULT '',
	FOREIGN KEY("collection") REFERENCES "AgLibraryCollection"("id_local"),
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgPendingOzAlbumAssetIds(
	ozCatalogId NOT NULL,
	ozAlbumAssetId NOT NULL,
	ozAssetId NOT NULL,
	ozAlbumId NOT NULL
);
CREATE TABLE AgPendingOzAlbumAssetIds_sortOrder(
	ozAlbumAssetId PRIMARY KEY,
	ozSortOrder NOT NULL
);
CREATE TABLE AgPendingOzAssetBinaryDownloads(
	ozAssetId NOT NULL,
	ozCatalogId NOT NULL,
	whenQueued NOT NULL,
	path NOT NULL,
	state DEFAULT "master"
);
CREATE TABLE AgPendingOzAssets(
	ozAssetId NOT NULL,
	ozCatalogId NOT NULL,
	state DEFAULT "needs_binary",
	path NOT NULL,
	payload NOT NULL
);
CREATE TABLE AgPhotoComment (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    comment,
    commentRealname,
    commentUsername,
    dateCreated,
    photo INTEGER NOT NULL DEFAULT 0,
    remoteId NOT NULL DEFAULT '',
    remotePhoto INTEGER,
    url
);
CREATE TABLE AgPhotoProperty (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    dataType,
    internalValue,
    photo INTEGER NOT NULL DEFAULT 0,
    propertySpec INTEGER NOT NULL DEFAULT 0
);
CREATE TABLE AgPhotoPropertyArrayElement (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    arrayIndex NOT NULL DEFAULT '',
    dataType,
    internalValue,
    photo INTEGER NOT NULL DEFAULT 0,
    propertySpec INTEGER NOT NULL DEFAULT 0
);
CREATE TABLE AgPhotoPropertySpec (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    flattenedAttributes,
    key NOT NULL DEFAULT '',
    pluginVersion NOT NULL DEFAULT '',
    sourcePlugin NOT NULL DEFAULT '',
    userVisibleName
);
CREATE TABLE AgPublishListenerWorklist (
    id_local INTEGER PRIMARY KEY,
    taskID UNIQUE NOT NULL DEFAULT '',
    taskStatus NOT NULL DEFAULT 'pending',
    whenPosted NOT NULL DEFAULT ''
);
CREATE TABLE AgRemotePhoto (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    collection INTEGER NOT NULL DEFAULT 0,
    commentCount,
    developSettingsDigest,
    fileContentsHash,
    fileModTimestamp,
    metadataDigest,
    mostRecentCommentTime,
    orientation,
    photo INTEGER NOT NULL DEFAULT 0,
    photoNeedsUpdating DEFAULT 2,
    publishCount,
    remoteId,
    serviceAggregateRating,
    url
);
CREATE TABLE AgSearchablePhotoProperty (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    dataType,
    internalValue,
    lc_idx_internalValue,
    photo INTEGER NOT NULL DEFAULT 0,
    propertySpec INTEGER NOT NULL DEFAULT 0
);
CREATE TABLE AgSearchablePhotoPropertyArrayElement (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    arrayIndex NOT NULL DEFAULT '',
    dataType,
    internalValue,
    lc_idx_internalValue,
    photo INTEGER NOT NULL DEFAULT 0,
    propertySpec INTEGER NOT NULL DEFAULT 0
);
CREATE TABLE AgSourceColorProfileConstants (
    id_local INTEGER PRIMARY KEY,
    image INTEGER NOT NULL DEFAULT 0,
    profileName NOT NULL DEFAULT 'Untagged',
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgSpecialSourceContent (
    id_local INTEGER PRIMARY KEY,
    content,
    owningModule,
    source NOT NULL DEFAULT ''
);
CREATE TABLE AgTempImages (
	image INTEGER PRIMARY KEY,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgVideoInfo (
    id_local INTEGER PRIMARY KEY,
    duration,
    frame_rate,
    has_audio INTEGER NOT NULL DEFAULT 1,
    has_video INTEGER NOT NULL DEFAULT 1,
    image INTEGER NOT NULL DEFAULT 0,
    poster_frame NOT NULL DEFAULT '0000000000000000/0000000000000001',
    poster_frame_set_by_user INTEGER NOT NULL DEFAULT 0,
    trim_end NOT NULL DEFAULT '0000000000000000/0000000000000001',
    trim_start NOT NULL DEFAULT '0000000000000000/0000000000000001',
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE LrMobileSyncChangeCounter(
	id PRIMARY KEY,
	changeCounter NOT NULL
);

CREATE TABLE "Adobe_AdditionalMetadata" (
	"id_local"	INTEGER,
	"id_global"	 NOT NULL UNIQUE,
	"additionalInfoSet"	INTEGER NOT NULL DEFAULT 0,
	"embeddedXmp"	INTEGER NOT NULL DEFAULT 0,
	"externalXmpIsDirty"	INTEGER NOT NULL DEFAULT 0,
	"image"	INTEGER,
	"incrementalWhiteBalance"	INTEGER NOT NULL DEFAULT 0,
	"internalXmpDigest"	,
	"isRawFile"	INTEGER NOT NULL DEFAULT 0,
	"lastSynchronizedHash"	,
	"lastSynchronizedTimestamp"	 NOT NULL DEFAULT -63113817600,
	"metadataPresetID"	,
	"metadataVersion"	,
	"monochrome"	INTEGER NOT NULL DEFAULT 0,
	"xmp"	 NOT NULL DEFAULT '',
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local"),
	PRIMARY KEY("id_local")
);
CREATE TABLE "Adobe_imageDevelopBeforeSettings" (
	"id_local"	INTEGER,
	"beforeDigest"	,
	"beforeHasDevelopAdjustments"	,
	"beforePresetID"	,
	"beforeText"	,
	"developSettings"	INTEGER,
	FOREIGN KEY("developSettings") REFERENCES "Adobe_imageDevelopSettings"("id_local"),
	PRIMARY KEY("id_local")
);
CREATE TABLE "Adobe_imageDevelopSettings" (
	"id_local"	INTEGER,
	"allowFastRender"	INTEGER,
	"beforeSettingsIDCache"	,
	"croppedHeight"	,
	"croppedWidth"	,
	"digest"	,
	"fileHeight"	,
	"fileWidth"	,
	"grayscale"	INTEGER,
	"hasDevelopAdjustments"	INTEGER,
	"hasDevelopAdjustmentsEx"	,
	"historySettingsID"	,
	"image"	INTEGER,
	"processVersion"	,
	"settingsID"	,
	"snapshotID"	,
	"text"	,
	"validatedForVersion"	,
	"whiteBalance"	,
	PRIMARY KEY("id_local"),
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE "Adobe_imageProofSettings" (
	"id_local"	INTEGER,
	"colorProfile"	,
	"image"	INTEGER,
	"renderingIntent"	,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local"),
	PRIMARY KEY("id_local")
);
CREATE TABLE "Adobe_images" (
	"id_local"	INTEGER,
	"id_global"	 NOT NULL UNIQUE,
	"aspectRatioCache"	 NOT NULL DEFAULT -1,
	"bitDepth"	 NOT NULL DEFAULT 0,
	"captureTime"	,
	"colorChannels"	 NOT NULL DEFAULT 0,
	"colorLabels"	 NOT NULL DEFAULT '',
	"colorMode"	 NOT NULL DEFAULT -1,
	"copyCreationTime"	 NOT NULL DEFAULT -63113817600,
	"copyName"	,
	"copyReason"	,
	"developSettingsIDCache"	,
	"fileFormat"	 NOT NULL DEFAULT 'unset',
	"fileHeight"	,
	"fileWidth"	,
	"hasMissingSidecars"	INTEGER,
	"masterImage"	INTEGER,
	"orientation"	,
	"originalCaptureTime"	,
	"originalRootEntity"	INTEGER,
	"panningDistanceH"	,
	"panningDistanceV"	,
	"pick"	 NOT NULL DEFAULT 0,
	"positionInFolder"	 NOT NULL DEFAULT 'z',
	"propertiesCache"	,
	"pyramidIDCache"	,
	"rating"	,
	"rootFile"	INTEGER NOT NULL DEFAULT 0,
	"sidecarStatus"	,
	"touchCount"	 NOT NULL DEFAULT 0,
	"touchTime"	 NOT NULL DEFAULT 0,
	FOREIGN KEY("rootFile") REFERENCES "AgLibraryFile"("id_local"),
	FOREIGN KEY("masterImage") REFERENCES "Adobe_images"("id_local"),
	PRIMARY KEY("id_local")
);
CREATE TABLE "AgLibraryFile" (
	"id_local"	INTEGER,
	"id_global"	 NOT NULL UNIQUE,
	"baseName"	 NOT NULL DEFAULT '',
	"errorMessage"	,
	"errorTime"	,
	"extension"	 NOT NULL DEFAULT '',
	"externalModTime"	,
	"folder"	INTEGER NOT NULL DEFAULT 0,
	"idx_filename"	 NOT NULL DEFAULT '',
	"importHash"	,
	"lc_idx_filename"	 NOT NULL DEFAULT '',
	"lc_idx_filenameExtension"	 NOT NULL DEFAULT '',
	"md5"	,
	"modTime"	,
	"originalFilename"	 NOT NULL DEFAULT '',
	"sidecarExtensions"	,
	FOREIGN KEY("folder") REFERENCES "AgLibraryFolder"("id_local"),
	PRIMARY KEY("id_local")
);
CREATE TABLE Adobe_faceProperties (
    id_local INTEGER PRIMARY KEY,
    face INTEGER,
    propertiesString,
	FOREIGN KEY("face") REFERENCES "AgLibraryFace"("id_local")
);
CREATE TABLE Adobe_imageProperties (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    image INTEGER,
    propertiesString,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE Adobe_libraryImageDevelopHistoryStep (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    dateCreated,
    digest,
    hasDevelopAdjustments,
    image INTEGER,
    name,
    relValueString,
    text,
    valueString,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE Adobe_libraryImageDevelopSnapshot (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    digest,
    hasDevelopAdjustments,
    image INTEGER,
    locked,
    name,
    snapshotID,
    text,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE Adobe_libraryImageFaceProcessHistory (
    id_local INTEGER PRIMARY KEY,
    image INTEGER NOT NULL DEFAULT 0,
    lastFaceDetector,
    lastFaceRecognizer,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgDNGProxyInfo (
    id_local INTEGER PRIMARY KEY,
    fileUUID NOT NULL DEFAULT '',
    status NOT NULL DEFAULT 'U',
    statusDateTime NOT NULL DEFAULT 0,
	FOREIGN KEY("fileUUID") REFERENCES "AgLibraryFile"("id_global")
);
CREATE TABLE AgFolderContent (
    id_local INTEGER PRIMARY KEY,
    id_global UNIQUE NOT NULL,
    containingFolder INTEGER NOT NULL DEFAULT 0,
    content,
    name,
    owningModule,
	FOREIGN KEY("containingFolder") REFERENCES "AgLibraryFolder"("id_local")
);
CREATE TABLE AgHarvestedDNGMetadata (
    id_local INTEGER PRIMARY KEY,
    image INTEGER,
    hasFastLoadData INTEGER,
    hasLossyCompression INTEGER,
    isDNG INTEGER,
    isReducedResolution INTEGER,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local")
);
CREATE TABLE AgHarvestedExifMetadata (
    id_local INTEGER PRIMARY KEY,
    image INTEGER,
    aperture,
    cameraModelRef INTEGER,
    cameraSNRef INTEGER,
    dateDay,
    dateMonth,
    dateYear,
    flashFired INTEGER,
    focalLength,
    gpsLatitude,
    gpsLongitude,
    gpsSequence NOT NULL DEFAULT 0,
    hasGPS INTEGER,
    isoSpeedRating,
    lensRef INTEGER,
    shutterSpeed,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local"),
	FOREIGN KEY("cameraModelRef") REFERENCES "AgInternedExifCameraModel"("id_local"),
	FOREIGN KEY("cameraSNRef") REFERENCES "AgInternedExifCameraSN"("id_local"),
	FOREIGN KEY("lensRef") REFERENCES "AgInternedExifLens"("id_local")
);
CREATE TABLE AgHarvestedIptcMetadata (
    id_local INTEGER PRIMARY KEY,
    image INTEGER,
    cityRef INTEGER,
    copyrightState INTEGER,
    countryRef INTEGER,
    creatorRef INTEGER,
    isoCountryCodeRef INTEGER,
    jobIdentifierRef INTEGER,
    locationDataOrigination NOT NULL DEFAULT 'unset',
    locationGPSSequence NOT NULL DEFAULT -1,
    locationRef INTEGER,
    stateRef INTEGER,
	FOREIGN KEY("image") REFERENCES "Adobe_images"("id_local"),
	FOREIGN KEY("cityRef") REFERENCES "AgInternedIptcCity"("id_local"),
	FOREIGN KEY("countryRef") REFERENCES "AgInternedIptcCountry"("id_local"),
	FOREIGN KEY("creatorRef") REFERENCES "AgInternedIptcCreator"("id_local"),
	FOREIGN KEY("isoCountryCodeRef") REFERENCES "AgInternedIptcIsoCountryCode"("id_local"),
	FOREIGN KEY("jobIdentifierRef") REFERENCES "AgInternedIptcJobIdentifier"("id_local"),
	FOREIGN KEY("locationRef") REFERENCES "AgInternedIptcLocation"("id_local"),
	FOREIGN KEY("stateRef") REFERENCES "AgInternedIptcState"("id_local")
);

-- INDEXES

CREATE INDEX "index_Adobe_AdditionalMetadata_imageAndStatus" ON "Adobe_AdditionalMetadata" (
	"image",
	"externalXmpIsDirty"
);
CREATE INDEX "index_Adobe_imageDevelopBeforeSettings_developSettings" ON "Adobe_imageDevelopBeforeSettings" (
	"developSettings"
);
CREATE INDEX "index_Adobe_imageDevelopSettings_digest" ON "Adobe_imageDevelopSettings" (
	"digest"
);
CREATE INDEX "index_Adobe_imageDevelopSettings_image" ON "Adobe_imageDevelopSettings" (
	"image"
);
CREATE INDEX "index_Adobe_imageProofSettings_image" ON "Adobe_imageProofSettings" (
	"image"
);
CREATE INDEX "index_Adobe_images_captureTime" ON "Adobe_images" (
	"captureTime"
);
CREATE INDEX "index_Adobe_images_masterImage" ON "Adobe_images" (
	"masterImage"
);
CREATE INDEX "index_Adobe_images_originalRootEntity" ON "Adobe_images" (
	"originalRootEntity"
);
CREATE INDEX "index_Adobe_images_ratingAndCaptureTime" ON "Adobe_images" (
	"rating",
	"captureTime"
);
CREATE INDEX "index_Adobe_images_rootFile" ON "Adobe_images" (
	"rootFile"
);
CREATE INDEX "index_AgLibraryFile_folder" ON "AgLibraryFile" (
	"folder"
);
CREATE INDEX "index_AgLibraryFile_importHash" ON "AgLibraryFile" (
	"importHash"
);
CREATE INDEX index_Adobe_faceProperties_face ON Adobe_faceProperties( face );
CREATE INDEX index_Adobe_imageProperties_image ON Adobe_imageProperties( image );
CREATE INDEX index_Adobe_libraryImageDevelopHistoryStep_imageDateCreated ON Adobe_libraryImageDevelopHistoryStep( image, dateCreated );
CREATE INDEX index_Adobe_libraryImageDevelopSnapshot_image ON Adobe_libraryImageDevelopSnapshot( image );
CREATE INDEX index_Adobe_libraryImageFaceProcessHistory_image ON Adobe_libraryImageFaceProcessHistory( image );
CREATE INDEX index_Adobe_namedIdentityPlate_description ON Adobe_namedIdentityPlate( description );
CREATE INDEX index_Adobe_namedIdentityPlate_identityPlateHash ON Adobe_namedIdentityPlate( identityPlateHash );
CREATE INDEX index_Adobe_variables_name ON Adobe_variables( name );
CREATE INDEX index_AgDNGProxyInfo_statusDateTimeForUUID ON AgDNGProxyInfo( status, statusDateTime, fileUUID );
CREATE INDEX index_AgDNGProxyInfo_uuidStatusDateTime ON AgDNGProxyInfo( fileUUID, status, statusDateTime );
CREATE INDEX index_AgDeletedOzAlbumAssetIds_changeCounter ON
	AgDeletedOzAlbumAssetIds( changeCounter, ozCatalogId, ozAlbumAssetId );
CREATE INDEX index_AgDeletedOzAlbumIds_changeCounter ON
	AgDeletedOzAlbumIds( changeCounter, ozCatalogId, ozAlbumId );
CREATE INDEX index_AgDeletedOzAssetIds_changeCounter ON
	AgDeletedOzAssetIds( changeCounter, ozCatalogId, ozAssetId );
CREATE INDEX index_AgFolderContent_containingFolder ON AgFolderContent( containingFolder );
CREATE INDEX index_AgFolderContent_owningModule ON AgFolderContent( owningModule );
CREATE INDEX index_AgHarvestedDNGMetadata_byImage ON AgHarvestedDNGMetadata( image, isDNG, hasFastLoadData, hasLossyCompression, isReducedResolution );
CREATE INDEX index_AgHarvestedDNGMetadata_hasFastLoadData ON AgHarvestedDNGMetadata( hasFastLoadData );
CREATE INDEX index_AgHarvestedDNGMetadata_hasLossyCompression ON AgHarvestedDNGMetadata( hasLossyCompression );
CREATE INDEX index_AgHarvestedDNGMetadata_isDNG ON AgHarvestedDNGMetadata( isDNG );
CREATE INDEX index_AgHarvestedDNGMetadata_isReducedResolution ON AgHarvestedDNGMetadata( isReducedResolution );
CREATE INDEX index_AgHarvestedExifMetadata_aperture ON AgHarvestedExifMetadata( aperture );
CREATE INDEX index_AgHarvestedExifMetadata_cameraModelRef ON AgHarvestedExifMetadata( cameraModelRef );
CREATE INDEX index_AgHarvestedExifMetadata_cameraSNRef ON AgHarvestedExifMetadata( cameraSNRef );
CREATE INDEX index_AgHarvestedExifMetadata_date ON AgHarvestedExifMetadata( dateYear, dateMonth, dateDay, image );
CREATE INDEX index_AgHarvestedExifMetadata_flashFired ON AgHarvestedExifMetadata( flashFired );
CREATE INDEX index_AgHarvestedExifMetadata_focalLength ON AgHarvestedExifMetadata( focalLength );
CREATE INDEX index_AgHarvestedExifMetadata_gpsCluster ON AgHarvestedExifMetadata( hasGPS, gpsLatitude, gpsLongitude, image );
CREATE INDEX index_AgHarvestedExifMetadata_image ON AgHarvestedExifMetadata( image );
CREATE INDEX index_AgHarvestedExifMetadata_isoSpeedRating ON AgHarvestedExifMetadata( isoSpeedRating );
CREATE INDEX index_AgHarvestedExifMetadata_lensRef ON AgHarvestedExifMetadata( lensRef );
CREATE INDEX index_AgHarvestedExifMetadata_shutterSpeed ON AgHarvestedExifMetadata( shutterSpeed );
CREATE INDEX index_AgHarvestedIptcMetadata_cityRef ON AgHarvestedIptcMetadata( cityRef );
CREATE INDEX index_AgHarvestedIptcMetadata_copyrightState ON AgHarvestedIptcMetadata( copyrightState );
CREATE INDEX index_AgHarvestedIptcMetadata_countryRef ON AgHarvestedIptcMetadata( countryRef );
CREATE INDEX index_AgHarvestedIptcMetadata_creatorRef ON AgHarvestedIptcMetadata( creatorRef );
CREATE INDEX index_AgHarvestedIptcMetadata_image ON AgHarvestedIptcMetadata( image );
CREATE INDEX index_AgHarvestedIptcMetadata_isoCountryCodeRef ON AgHarvestedIptcMetadata( isoCountryCodeRef );
CREATE INDEX index_AgHarvestedIptcMetadata_jobIdentifierRef ON AgHarvestedIptcMetadata( jobIdentifierRef );
CREATE INDEX index_AgHarvestedIptcMetadata_locationRef ON AgHarvestedIptcMetadata( locationRef );
CREATE INDEX index_AgHarvestedIptcMetadata_stateRef ON AgHarvestedIptcMetadata( stateRef );
CREATE INDEX index_AgHarvestedMetadataWorklist_statusCluster ON AgHarvestedMetadataWorklist( taskStatus, whenPosted, taskID );
CREATE INDEX index_AgHarvestedMetadataWorklist_taskIDCluster ON AgHarvestedMetadataWorklist( taskID, whenPosted, taskStatus );
CREATE INDEX index_AgInternedExifCameraModel_searchIndex ON AgInternedExifCameraModel( searchIndex );
CREATE INDEX index_AgInternedExifCameraModel_value ON AgInternedExifCameraModel( value );
CREATE INDEX index_AgInternedExifCameraSN_searchIndex ON AgInternedExifCameraSN( searchIndex );
CREATE INDEX index_AgInternedExifCameraSN_value ON AgInternedExifCameraSN( value );
CREATE INDEX index_AgInternedExifLens_searchIndex ON AgInternedExifLens( searchIndex );
CREATE INDEX index_AgInternedExifLens_value ON AgInternedExifLens( value );
CREATE INDEX index_AgInternedIptcCity_searchIndex ON AgInternedIptcCity( searchIndex );
CREATE INDEX index_AgInternedIptcCity_value ON AgInternedIptcCity( value );
CREATE INDEX index_AgInternedIptcCountry_searchIndex ON AgInternedIptcCountry( searchIndex );
CREATE INDEX index_AgInternedIptcCountry_value ON AgInternedIptcCountry( value );
CREATE INDEX index_AgInternedIptcCreator_searchIndex ON AgInternedIptcCreator( searchIndex );
CREATE INDEX index_AgInternedIptcCreator_value ON AgInternedIptcCreator( value );
CREATE INDEX index_AgInternedIptcIsoCountryCode_searchIndex ON AgInternedIptcIsoCountryCode( searchIndex );
CREATE INDEX index_AgInternedIptcIsoCountryCode_value ON AgInternedIptcIsoCountryCode( value );
CREATE INDEX index_AgInternedIptcJobIdentifier_searchIndex ON AgInternedIptcJobIdentifier( searchIndex );
CREATE INDEX index_AgInternedIptcJobIdentifier_value ON AgInternedIptcJobIdentifier( value );
CREATE INDEX index_AgInternedIptcLocation_searchIndex ON AgInternedIptcLocation( searchIndex );
CREATE INDEX index_AgInternedIptcLocation_value ON AgInternedIptcLocation( value );
CREATE INDEX index_AgInternedIptcState_searchIndex ON AgInternedIptcState( searchIndex );
CREATE INDEX index_AgInternedIptcState_value ON AgInternedIptcState( value );
CREATE INDEX index_AgLibraryCollectionChangeCounter_changeCounter ON
	AgLibraryCollectionChangeCounter( changeCounter, collection );
CREATE INDEX index_AgLibraryCollectionContent_collection ON AgLibraryCollectionContent( collection );
CREATE INDEX index_AgLibraryCollectionContent_owningModule ON AgLibraryCollectionContent( owningModule );
CREATE INDEX index_AgLibraryCollectionImageChangeCounter_changeCounter ON
	AgLibraryCollectionImageChangeCounter( changeCounter, collectionImage,
											collection, image );
CREATE INDEX index_AgLibraryCollectionImageOzAlbumAssetIds_collectionAlbumAssetsLookup ON
	AgLibraryCollectionImageOzAlbumAssetIds( collection, ozCatalogId );
CREATE INDEX index_AgLibraryCollectionImageOzAlbumAssetIds_collectionFromAlbumAssetIdLookup ON
	AgLibraryCollectionImageOzAlbumAssetIds( ozAlbumAssetId, ozCatalogId,
												collectionImage, collection,
												image );
CREATE INDEX index_AgLibraryCollectionImageOzAlbumAssetIds_imageAlbumAssetsLookup ON
	AgLibraryCollectionImageOzAlbumAssetIds( image, ozCatalogId );
CREATE INDEX index_AgLibraryCollectionImage_collection ON AgLibraryCollectionImage( collection );
CREATE INDEX index_AgLibraryCollectionImage_imageCollection ON AgLibraryCollectionImage( image, collection );
CREATE INDEX index_AgLibraryCollectionOzAlbumIds_catalogAlbumsLookup ON
	AgLibraryCollectionOzAlbumIds( ozCatalogId, ozAlbumId, collection );
CREATE INDEX index_AgLibraryCollectionOzAlbumIds_collectionFromAlbumIdLookup ON
	AgLibraryCollectionOzAlbumIds( ozAlbumId, ozCatalogId, collection );
CREATE INDEX index_AgLibraryCollectionStackData ON AgLibraryCollectionStackData( stack, collection, stackCount, stackParent );
CREATE INDEX index_AgLibraryCollectionStackImage_getStackFromImage ON AgLibraryCollectionStackImage( collection, image, stack, position, collapsed );
CREATE INDEX index_AgLibraryCollectionStackImage_image ON AgLibraryCollectionStackImage( image );
CREATE INDEX index_AgLibraryCollectionStackImage_orderByCollapseThenStackThenPosition ON AgLibraryCollectionStackImage( collection, collapsed, stack, position, image );
CREATE INDEX index_AgLibraryCollectionStackImage_orderByPositionThenStack ON AgLibraryCollectionStackImage( collection, position, stack, image, collapsed );
CREATE INDEX index_AgLibraryCollectionStackImage_orderByStackThenPosition ON AgLibraryCollectionStackImage( collection, stack, position, image, collapsed );
CREATE INDEX index_AgLibraryCollectionStackImage_stack ON AgLibraryCollectionStackImage( stack );
CREATE INDEX index_AgLibraryCollectionStack_stacksForCollection ON AgLibraryCollectionStack( collection, collapsed );
CREATE INDEX index_AgLibraryCollectionTrackedAssets_byOzCatalogId ON
	AgLibraryCollectionTrackedAssets( ozCatalogId, collection );
CREATE INDEX index_AgLibraryCollection_genealogy ON AgLibraryCollection( genealogy );
CREATE INDEX index_AgLibraryCollection_parentAndName ON AgLibraryCollection( parent, name );
CREATE INDEX index_AgLibraryFaceCluster_keyFace ON AgLibraryFaceCluster( keyFace );
CREATE INDEX index_AgLibraryFaceData_face ON AgLibraryFaceData( face );
CREATE INDEX index_AgLibraryFace_cluster ON AgLibraryFace( cluster );
CREATE INDEX index_AgLibraryFace_image ON AgLibraryFace( image );
CREATE INDEX index_AgLibraryFileAssetMetadata_sha256ToFileId  ON
	AgLibraryFileAssetMetadata( sha256, fileSize );
CREATE INDEX index_AgLibraryFolderStackData ON AgLibraryFolderStackData( stack, stackCount, stackParent );
CREATE INDEX index_AgLibraryFolderStackImage_getStackFromImage ON AgLibraryFolderStackImage( image, stack, position, collapsed );
CREATE INDEX index_AgLibraryFolderStackImage_orderByCollapseThenStackThenPosition ON AgLibraryFolderStackImage( collapsed, stack, position, image );
CREATE INDEX index_AgLibraryFolderStackImage_orderByPositionThenStack ON AgLibraryFolderStackImage( position, stack, image, collapsed );
CREATE INDEX index_AgLibraryFolderStackImage_orderByStackThenPosition ON AgLibraryFolderStackImage( stack, position, image, collapsed );
CREATE INDEX index_AgLibraryFolderStack_collapsed ON AgLibraryFolderStack( collapsed );
CREATE INDEX index_AgLibraryIPTC_image ON AgLibraryIPTC( image );
CREATE INDEX index_AgLibraryImageChangeCounter_changeCounter ON
	AgLibraryImageChangeCounter( changeCounter, image );
CREATE INDEX index_AgLibraryImageOzAssetIds_imageFromAssetIdLookup ON
	AgLibraryImageOzAssetIds( ozAssetId, ozCatalogId, image, ownedByCatalog );
CREATE INDEX index_AgLibraryImageXMPUpdater_statusCluster ON AgLibraryImageXMPUpdater( taskStatus, whenPosted, taskID );
CREATE INDEX index_AgLibraryImageXMPUpdater_taskIDCluster ON AgLibraryImageXMPUpdater( taskID, whenPosted, taskStatus );
CREATE INDEX index_AgLibraryImportImage_import ON AgLibraryImportImage( import );
CREATE INDEX index_AgLibraryKeywordCooccurrence_tag1Search ON AgLibraryKeywordCooccurrence( tag1, value, tag2 );
CREATE INDEX index_AgLibraryKeywordCooccurrence_tagsLookup ON AgLibraryKeywordCooccurrence( tag1, tag2 );
CREATE INDEX index_AgLibraryKeywordCooccurrence_valueIndex ON AgLibraryKeywordCooccurrence( value );
CREATE INDEX index_AgLibraryKeywordFace_face ON AgLibraryKeywordFace( face );
CREATE INDEX index_AgLibraryKeywordFace_tag ON AgLibraryKeywordFace( tag );
CREATE INDEX index_AgLibraryKeywordImage_image ON AgLibraryKeywordImage( image );
CREATE INDEX index_AgLibraryKeywordImage_tag ON AgLibraryKeywordImage( tag );
CREATE INDEX index_AgLibraryKeywordPopularity_popularity ON AgLibraryKeywordPopularity( popularity );
CREATE INDEX index_AgLibraryKeywordSynonym_keyword ON AgLibraryKeywordSynonym( keyword );
CREATE INDEX index_AgLibraryKeywordSynonym_lc_name ON AgLibraryKeywordSynonym( lc_name );
CREATE INDEX index_AgLibraryKeyword_genealogy ON AgLibraryKeyword( genealogy );
CREATE INDEX index_AgLibraryKeyword_parentAndLcName ON AgLibraryKeyword( parent, lc_name );
CREATE INDEX index_AgLibraryOzFeedbackInfo_lastFeedbackTime ON AgLibraryOzFeedbackInfo( lastFeedbackTime );
CREATE INDEX index_AgLibraryPublishedCollectionContent_collection ON AgLibraryPublishedCollectionContent( collection );
CREATE INDEX index_AgLibraryPublishedCollectionContent_owningModule ON AgLibraryPublishedCollectionContent( owningModule );
CREATE INDEX index_AgLibraryPublishedCollectionImage_collection ON AgLibraryPublishedCollectionImage( collection );
CREATE INDEX index_AgLibraryPublishedCollectionImage_imageCollection ON AgLibraryPublishedCollectionImage( image, collection );
CREATE INDEX index_AgLibraryPublishedCollection_genealogy ON AgLibraryPublishedCollection( genealogy );
CREATE INDEX index_AgLibraryPublishedCollection_parentAndName ON AgLibraryPublishedCollection( parent, name );
CREATE INDEX index_AgMRULists_listID ON AgMRULists( listID );
CREATE INDEX index_AgMetadataSearchIndex_image ON AgMetadataSearchIndex( image );
CREATE INDEX index_AgOutputImageAsset_findByCollectionGroupByImage ON AgOutputImageAsset( moduleId, collection, image, assetId );
CREATE INDEX index_AgOutputImageAsset_findByCollectionImage ON AgOutputImageAsset( collection, image, moduleId, assetId );
CREATE INDEX index_AgOutputImageAsset_image ON AgOutputImageAsset( image );
CREATE INDEX index_AgPendingOzAlbumAssetIds_byAlbumAssetId ON
	AgPendingOzAlbumAssetIds( ozAlbumAssetId, ozAssetId, ozCatalogId, ozAlbumId );
CREATE INDEX index_AgPendingOzAlbumAssetIds_byAlbumId ON
	AgPendingOzAlbumAssetIds( ozAlbumId, ozCatalogId, ozAlbumAssetId, ozAssetId );
CREATE INDEX index_AgPendingOzAlbumAssetIds_byAssetId ON
	AgPendingOzAlbumAssetIds( ozAssetId, ozCatalogId, ozAlbumAssetId, ozAlbumId );
CREATE INDEX index_AgPendingOzAssetBinaryDownloads_catalogIdOrdering ON
	AgPendingOzAssetBinaryDownloads( ozCatalogId, whenQueued, state, ozAssetId, path );
CREATE INDEX index_AgPendingOzAssetBinaryDownloads_stateSearches ON
	AgPendingOzAssetBinaryDownloads( state, ozCatalogId, whenQueued, ozAssetId );
CREATE INDEX index_AgPendingOzAssets_stateSearches ON
	AgPendingOzAssets( state, ozCatalogId, ozAssetId );
CREATE INDEX index_AgPhotoComment_photo ON AgPhotoComment( photo );
CREATE INDEX index_AgPhotoComment_remoteId ON AgPhotoComment( remoteId );
CREATE INDEX index_AgPhotoComment_remotePhoto ON AgPhotoComment( remotePhoto );
CREATE INDEX index_AgPhotoPropertyArrayElement_propertySpec ON AgPhotoPropertyArrayElement( propertySpec );
CREATE INDEX index_AgPhotoProperty_propertySpec ON AgPhotoProperty( propertySpec );
CREATE INDEX index_AgPublishListenerWorklist_statusCluster ON AgPublishListenerWorklist( taskStatus, whenPosted, taskID );
CREATE INDEX index_AgPublishListenerWorklist_taskIDCluster ON AgPublishListenerWorklist( taskID, whenPosted, taskStatus );
CREATE INDEX index_AgRemotePhoto_collectionNeedsUpdating ON AgRemotePhoto( collection, photoNeedsUpdating );
CREATE INDEX index_AgRemotePhoto_photo ON AgRemotePhoto( photo );
CREATE INDEX index_AgSearchablePhotoPropertyArrayElement_lc_idx_internalValue ON AgSearchablePhotoPropertyArrayElement( lc_idx_internalValue );
CREATE INDEX index_AgSearchablePhotoPropertyArrayElement_propertyValue ON AgSearchablePhotoPropertyArrayElement( propertySpec, internalValue );
CREATE INDEX index_AgSearchablePhotoPropertyArrayElement_propertyValue_lc ON AgSearchablePhotoPropertyArrayElement( propertySpec, lc_idx_internalValue );
CREATE INDEX index_AgSearchablePhotoProperty_lc_idx_internalValue ON AgSearchablePhotoProperty( lc_idx_internalValue );
CREATE INDEX index_AgSearchablePhotoProperty_propertyValue ON AgSearchablePhotoProperty( propertySpec, internalValue );
CREATE INDEX index_AgSearchablePhotoProperty_propertyValue_lc ON AgSearchablePhotoProperty( propertySpec, lc_idx_internalValue );
CREATE INDEX index_AgSourceColorProfileConstants_image ON AgSourceColorProfileConstants( image );
CREATE INDEX index_AgSourceColorProfileConstants_imageSourceColorProfileName ON AgSourceColorProfileConstants( profileName, image );
CREATE INDEX index_AgSpecialSourceContent_owningModule ON AgSpecialSourceContent( owningModule );
CREATE INDEX index_AgVideoInfo_image ON AgVideoInfo( image );

CREATE UNIQUE INDEX "index_AgLibraryFile_nameAndFolder" ON "AgLibraryFile" (
	"lc_idx_filename",
	"folder"
);
CREATE UNIQUE INDEX "index_AgLibraryFolder_rootFolderAndPath" ON "AgLibraryFolder" (
	"rootFolder",
	"pathFromRoot"
);
CREATE UNIQUE INDEX index_AgDeletedOzAlbumAssetIds_primaryKey ON
	AgDeletedOzAlbumAssetIds( ozCatalogId, ozAlbumAssetId );
CREATE UNIQUE INDEX index_AgDeletedOzAlbumIds_primaryKey ON
	AgDeletedOzAlbumIds( ozCatalogId, ozAlbumId );
CREATE UNIQUE INDEX index_AgDeletedOzAssetIds_primaryKey ON
	AgDeletedOzAssetIds( ozCatalogId, ozAssetId );
CREATE UNIQUE INDEX index_AgLibraryCollectionImageOzAlbumAssetIds_primaryKey ON
	AgLibraryCollectionImageOzAlbumAssetIds( collectionImage, ozCatalogId,
												collection, image );
CREATE UNIQUE INDEX index_AgLibraryCollectionOzAlbumIds_primaryKey ON
	AgLibraryCollectionOzAlbumIds( collection, ozCatalogId );
CREATE UNIQUE INDEX index_AgLibraryCollectionSyncedAlbumData_primaryKey ON
	AgLibraryCollectionSyncedAlbumData( collection, payloadKey );
CREATE UNIQUE INDEX index_AgLibraryCollectionTrackedAssets_primaryKey ON
	AgLibraryCollectionTrackedAssets( collection, ozCatalogId );
CREATE UNIQUE INDEX index_AgLibraryImageOzAssetIds_primaryKey ON
	AgLibraryImageOzAssetIds( image, ozCatalogId );
CREATE UNIQUE INDEX index_AgLibraryImageSyncedAssetData_primaryKey ON
	AgLibraryImageSyncedAssetData( image, payloadKey );
CREATE UNIQUE INDEX index_AgLibraryImportImage_imageAndImport ON AgLibraryImportImage( image, import );
CREATE UNIQUE INDEX index_AgLibraryOzFeedbackInfo_assetAndSpaceAndCatalog ON AgLibraryOzFeedbackInfo( ozAssetId, ozSpaceId, ozCatalogId );
CREATE UNIQUE INDEX index_AgPendingOzAlbumAssetIds_primaryKey ON
	AgPendingOzAlbumAssetIds( ozCatalogId, ozAlbumAssetId, ozAssetId, ozAlbumId );
CREATE UNIQUE INDEX index_AgPendingOzAssetBinaryDownloads_primaryKey ON
	AgPendingOzAssetBinaryDownloads( ozAssetId, ozCatalogId );
CREATE UNIQUE INDEX index_AgPendingOzAssets_primaryKey ON
	AgPendingOzAssets( ozAssetId, ozCatalogId );
CREATE UNIQUE INDEX index_AgPhotoPropertyArrayElement_pluginKey ON AgPhotoPropertyArrayElement( photo, propertySpec, arrayIndex );
CREATE UNIQUE INDEX index_AgPhotoPropertySpec_pluginKey ON AgPhotoPropertySpec( sourcePlugin, key, pluginVersion );
CREATE UNIQUE INDEX index_AgPhotoProperty_pluginKey ON AgPhotoProperty( photo, propertySpec );
CREATE UNIQUE INDEX index_AgRemotePhoto_collectionPhoto ON AgRemotePhoto( collection, photo );
CREATE UNIQUE INDEX index_AgRemotePhoto_collectionRemoteId ON AgRemotePhoto( collection, remoteId );
CREATE UNIQUE INDEX index_AgSearchablePhotoPropertyArrayElement_pluginKey ON AgSearchablePhotoPropertyArrayElement( photo, propertySpec, arrayIndex );
CREATE UNIQUE INDEX index_AgSearchablePhotoProperty_pluginKey ON AgSearchablePhotoProperty( photo, propertySpec );
CREATE UNIQUE INDEX index_AgSpecialSourceContent_sourceModule ON AgSpecialSourceContent( source, owningModule );

-- TRIGGER

CREATE TRIGGER AgLibraryCollectionStackData_delete
								AFTER DELETE ON AgLibraryCollectionStack
								FOR EACH ROW
								BEGIN
									DELETE FROM AgLibraryCollectionStackData WHERE stack = OLD.id_local AND collection = OLD.collection;
								END;
CREATE TRIGGER AgLibraryCollectionStackData_init
								AFTER INSERT ON AgLibraryCollectionStack
								FOR EACH ROW
								BEGIN
									INSERT INTO AgLibraryCollectionStackData( stack, collection ) VALUES( NEW.id_local, NEW.collection );
								END;
CREATE TRIGGER AgLibraryCollectionStackData_initStackData
	AFTER INSERT ON AgLibraryCollectionStackData
	FOR EACH ROW
	BEGIN
		UPDATE AgLibraryCollectionStackData
			SET stackCount = (
				SELECT COUNT( * ) FROM AgLibraryCollectionStackImage
				WHERE stack = NEW.stack AND collection = NEW.collection )
			WHERE stack = NEW.stack AND collection = NEW.collection;
		UPDATE AgLibraryCollectionStackData
			SET stackParent = (
				SELECT image FROM AgLibraryCollectionStackImage
				WHERE stack = NEW.stack AND collection = NEW.collection AND
				      position = 1 )
			WHERE stack = NEW.stack AND collection = NEW.collection;
	END;
CREATE TRIGGER AgLibraryCollectionStackImage_propagateStackParentOnAdd
								   AFTER INSERT ON AgLibraryCollectionStackImage
								   FOR EACH ROW WHEN NEW.position = 1
								   BEGIN
										UPDATE AgLibraryCollectionStackData
										SET stackParent = NEW.image
										WHERE stack = NEW.stack AND
										      collection = NEW.collection;
									END;
CREATE TRIGGER AgLibraryCollectionStackImage_propagateStackParentOnUpdate
								   AFTER UPDATE OF position ON AgLibraryCollectionStackImage
								   FOR EACH ROW WHEN NEW.position = 1
								   BEGIN
										UPDATE AgLibraryCollectionStackData
										SET stackParent = NEW.image
										WHERE stack = NEW.stack AND
										      collection = NEW.collection;
									END;
CREATE TRIGGER AgLibraryCollectionStackImage_updateCountOnAdd
								   AFTER INSERT ON AgLibraryCollectionStackImage
								   FOR EACH ROW
								   BEGIN
										UPDATE AgLibraryCollectionStackData
										SET stackCount = stackCount + 1
										WHERE stack = NEW.stack AND
										      collection = NEW.collection;
									END;
CREATE TRIGGER AgLibraryCollectionStackImage_updateCountOnDelete
								   AFTER DELETE ON AgLibraryCollectionStackImage
								   FOR EACH ROW
								   BEGIN
										UPDATE AgLibraryCollectionStackData
										SET stackCount = MAX( 0, stackCount - 1 )
										WHERE stack = OLD.stack AND
										      collection = OLD.collection;
									END;
CREATE TRIGGER AgLibraryCollectionStack_propagateCollapseState
								   AFTER UPDATE OF collapsed ON AgLibraryCollectionStack
								   FOR EACH ROW
								   BEGIN
										UPDATE AgLibraryCollectionStackImage
										SET collapsed = NEW.collapsed
										WHERE AgLibraryCollectionStackImage.stack = NEW.id_local;
									END;
CREATE TRIGGER AgLibraryFolderStackData_delete
								AFTER DELETE ON AgLibraryFolderStack
								FOR EACH ROW
								BEGIN
									DELETE FROM AgLibraryFolderStackData WHERE stack = OLD.id_local;
								END;
CREATE TRIGGER AgLibraryFolderStackData_init
								AFTER INSERT ON AgLibraryFolderStack
								FOR EACH ROW
								BEGIN
									INSERT INTO AgLibraryFolderStackData( stack ) VALUES( NEW.id_local );
								END;
CREATE TRIGGER AgLibraryFolderStackData_initStackData
	AFTER INSERT ON AgLibraryFolderStackData
	FOR EACH ROW
	BEGIN
		UPDATE AgLibraryFolderStackData
			SET stackCount = (
				SELECT COUNT( * ) FROM AgLibraryFolderStackImage
				WHERE stack = NEW.stack )
			WHERE stack = NEW.stack;
		UPDATE AgLibraryFolderStackData
			SET stackParent = (
				SELECT image FROM AgLibraryFolderStackImage
				WHERE stack = NEW.stack AND position = 1 )
			WHERE stack = NEW.stack;

	END;
CREATE TRIGGER AgLibraryFolderStackImage_propagateStackParentOnAdd
								   AFTER INSERT ON AgLibraryFolderStackImage
								   FOR EACH ROW WHEN NEW.position = 1
								   BEGIN
										UPDATE AgLibraryFolderStackData
										SET stackParent = NEW.image
										WHERE stack = NEW.stack;
									END;
CREATE TRIGGER AgLibraryFolderStackImage_propagateStackParentOnUpdate
								   AFTER UPDATE OF position ON AgLibraryFolderStackImage
								   FOR EACH ROW WHEN NEW.position = 1
								   BEGIN
										UPDATE AgLibraryFolderStackData
										SET stackParent = NEW.image
										WHERE stack = NEW.stack;
									END;
CREATE TRIGGER AgLibraryFolderStackImage_updateCountOnAdd
								   AFTER INSERT ON AgLibraryFolderStackImage
								   FOR EACH ROW
								   BEGIN
										UPDATE AgLibraryFolderStackData
										SET stackCount = stackCount + 1
										WHERE stack = NEW.stack;
									END;
CREATE TRIGGER AgLibraryFolderStackImage_updateStackCountOnDelete
								   AFTER DELETE ON AgLibraryFolderStackImage
								   FOR EACH ROW
								   BEGIN
										UPDATE AgLibraryFolderStackData
										SET stackCount = MAX( 0, stackCount - 1 )
										WHERE stack = OLD.stack;
									END;
CREATE TRIGGER AgLibraryFolderStack_propagateCollapseState
								   AFTER UPDATE OF collapsed ON AgLibraryFolderStack
								   FOR EACH ROW
								   BEGIN
										UPDATE AgLibraryFolderStackImage
										SET collapsed = NEW.collapsed
										WHERE AgLibraryFolderStackImage.stack = NEW.id_local;
									END;
CREATE TRIGGER trigger_AgDNGProxyInfo_fileDeleted
											AFTER DELETE ON AgLibraryFile
											FOR EACH ROW
											BEGIN
												UPDATE AgDNGProxyInfo
												SET status = 'D', statusDateTime = datetime( 'now' )
												WHERE fileUUID = OLD.id_global;
											END;
CREATE TRIGGER trigger_AgDNGProxyInfo_fileInserted
						   					 AFTER INSERT ON AgLibraryFile
											 FOR EACH ROW
											 BEGIN
												UPDATE AgDNGProxyInfo
												SET status = 'U', statusDateTime = datetime( 'now' )
												WHERE fileUUID = NEW.id_global;
											END;
CREATE TRIGGER trigger_AgLibraryCollectionCoverImage_delete_collection
	AFTER DELETE on AgLibraryCollection
	FOR EACH ROW
	BEGIN

		DELETE FROM AgLibraryCollectionCoverImage
		WHERE collection = OLD.id_local;

	END;
CREATE TRIGGER trigger_AgLibraryCollectionCoverImage_delete_collectionImage
	AFTER DELETE on AgLibraryCollectionImage
	FOR EACH ROW
	BEGIN


		DELETE FROM AgLibraryCollectionCoverImage
		WHERE collection = OLD.collection AND collectionImage = OLD.id_local;

	END;

COMMIT;
