﻿using System;
using System.Collections.Generic;
using System.Linq;
using imBlickFeld.Lightroom.Conditions;
using imBlickFeld.Lightroom.Loggers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace imBlickFeld.Lightroom.Test.Interop
{
    [TestClass]
    public class CatalogServiceGetFilenamesTests : ILogger
    {
        public void Log(string msg) {
            Console.WriteLine(msg);
        }

        [TestMethod]
        public void TestGetFileNamesByRating() {
            var expected = new List<string>() { "2019.060.336.dng", "2019.060.360.dng" };
            using (var service = new CatalogService(TestHelper.LR_CAT, new CatalogServiceOptions { DebugLogger = this, TraceLogger = this })) {
                var conditions = new ImageRating(Operator.Eq, 4);
                var actual = service.QueryFileNames(conditions)
                    .Select(x => x.FileName)
                    .ToList();
                CollectionAssert.AreEqual(expected, actual);
            }
        }
    }
}
