﻿using System;
using System.Collections.Generic;
using System.Linq;
using imBlickFeld.Lightroom.Conditions;
using imBlickFeld.Lightroom.Loggers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace imBlickFeld.Lightroom.Test.Interop
{
    [TestClass]
    public class CatalogServiceMetaDataTests : ILogger
    {
        public void Log(string msg) {
            Console.WriteLine(msg);
        }

        [TestMethod]
        public void TestGetMetaDateList() {
            var expected = new List<string>() { "3968645|2", "3968700|1" };
            using (var service = new CatalogService(TestHelper.LR_CAT, new CatalogServiceOptions { DebugLogger = this, TraceLogger = this })) {
                var conditions = new Directory(Operator.Like, "%2020.001%/");
                var actual = service.QueryMetadataForFiles(conditions)
                    .OrderBy(x => x.Key)
                    .Select(x => x.Key + "|" + x.Value.Count)
                    .ToList();
                CollectionAssert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void TestGetMetaDateTitle() {
            using (var service = new CatalogService(TestHelper.LR_CAT, new CatalogServiceOptions { DebugLogger = this, TraceLogger = this })) {
                const int idWithTitle = 3968665;
                var conditions = new NumericCondition("Adobe_AdditionalMetadata.id_local", Operator.Eq, idWithTitle);
                var md = service.QueryMetadataForFiles(conditions).Single().Value.Single();
                Assert.AreEqual("Titel nicht löschen!", md.Title);

                const int idNoTitle = 3968649;
                conditions = new NumericCondition("Adobe_AdditionalMetadata.id_local", Operator.Eq, idNoTitle);
                md = service.QueryMetadataForFiles(conditions).Single().Value.Single();
                Assert.IsNull(md.Title);

                md.Title = "Mein Titel";
                Assert.AreEqual("Mein Titel", md.Title);
            }
        }
    }
}
