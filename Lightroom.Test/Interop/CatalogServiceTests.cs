﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace imBlickFeld.Lightroom.Test.Interop
{
    [TestClass]
    public class CatalogServiceTests
    {
        [TestMethod]
        public void TestWorkOnCopy() {
            var options = new CatalogServiceOptions {
                WorkOnCopy = true,
                WorkingCopyPath = "TestTemp.lrcat.tmp"
            };
            using (var service = new CatalogService(TestHelper.LR_CAT, options)) {
                Assert.AreEqual(options.WorkingCopyPath, service.LRCatFilename);
                Assert.IsTrue(File.Exists(options.WorkingCopyPath), $"The file {options.WorkingCopyPath} should have been created, but does not exist.");
            }
            Assert.IsFalse(File.Exists(options.WorkingCopyPath), $"The file {options.WorkingCopyPath} should have been deleted, but still exists.");
        }

        [TestMethod]
        public void TestWorkOnCopySame() {
            var options = new CatalogServiceOptions {
                WorkOnCopy = true,
                WorkingCopyPath = TestHelper.LR_CAT
            };
            try {
                using (var service = new CatalogService(TestHelper.LR_CAT, options)) { }
            } catch (CatalogException) {
                return; // Expected
            }
            Assert.Fail("A CatalogException should have been thrown, but wasn't.");
        }
    }
}
