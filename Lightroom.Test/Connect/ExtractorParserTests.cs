﻿using imBlickFeld.Lightroom.Connect.Connectors.Extractors;
using imBlickFeld.Lightroom.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace imBlickFeld.Lightroom.Test.Connect
{
    [TestClass]
    public class ExtractorParserTests
    {
        [TestMethod]
        public void TestParseLiteralExtractorA() {
            var list = FieldExtractorFactory.I.FromString("a");
            Assert.AreEqual(1, list.Count);
            var res = list[0];
            Assert.IsInstanceOfType(res, typeof(LiteralExtractor));
            Assert.AreEqual("a", (res as LiteralExtractor).Value);
        }

        [TestMethod]
        public void TestParseLiteralExtractorAbc() {
            var list = FieldExtractorFactory.I.FromString("abc");
            Assert.AreEqual(1, list.Count);
            var res = list[0];
            Assert.IsInstanceOfType(res, typeof(LiteralExtractor));
            Assert.AreEqual("abc", (res as LiteralExtractor).Value);
        }

        [TestMethod]
        public void TestParseRegexFieldExtractor() {
            var list = FieldExtractorFactory.I.FromString(@"{!directory!(\d{4}\.\d{3})[^/]*/$!\1}");
            Assert.AreEqual(1, list.Count);
            var res = list[0];
            Assert.IsInstanceOfType(res, typeof(RegexFieldExtractor));
            var r = res as RegexFieldExtractor;
            Assert.AreEqual("directory", r.FieldName);
            Assert.AreEqual(@"(\d{4}\.\d{3})[^/]*/$", r.SearchRegex.ToString());
        }

        [TestMethod]
        public void TestParseRegexFieldExtractorSyntaxError() {
            Assert.ThrowsException<ArgumentException>(() => FieldExtractorFactory.I.FromString(@"{!directory!(\d{4}\.\d{3})[^/]*/$!\1"));
        }

        [TestMethod]
        public void TestParseLiteralRegexLiteral() {
            var list = FieldExtractorFactory.I.FromString(@"a{!directory!(\d{4}\.\d{3})[^/]*/$!\1}xyz");
            Assert.AreEqual(3, list.Count);
            var res = list[0];
            Assert.IsInstanceOfType(res, typeof(LiteralExtractor));
            Assert.AreEqual("a", (res as LiteralExtractor).Value);
            res = list[1];
            Assert.IsInstanceOfType(res, typeof(RegexFieldExtractor));
            res = list[2];
            Assert.IsInstanceOfType(res, typeof(LiteralExtractor));
            Assert.AreEqual("xyz", (res as LiteralExtractor).Value);
        }

        [TestMethod]
        public void TestParseRegexLiteralRegex() {
            var list = FieldExtractorFactory.I.FromString(@"{!directory!(\d{4}\.\d{3})[^/]*/$!\1}xyz{!directory!(\d{4}\.\d{3})[^/]*/$!\1}");
            Assert.AreEqual(3, list.Count);
            var res = list[0];
            Assert.IsInstanceOfType(res, typeof(RegexFieldExtractor));
            res = list[1];
            Assert.IsInstanceOfType(res, typeof(LiteralExtractor));
            Assert.AreEqual("xyz", (res as LiteralExtractor).Value);
            res = list[2];
            Assert.IsInstanceOfType(res, typeof(RegexFieldExtractor));
        }

        [TestMethod]
        public void TestMultipleLiteralAndRegexFieldExtractors() {
            var files = new[] {
                new LRFile {
                    Directory = "Sub/Directory/2020.001 Test/",
                    BaseName = "IMG_2851",
                    Extension = "JPG",
                },
                new LRFile {
                    Directory = "Sub/Directory/2020.001 Test/",
                    BaseName = "IMG_2852",
                    Extension = "JPG",
                },
                new LRFile {
                    Directory = "Sub/Directory/2020.001 Test/",
                    BaseName = "IMG_2851a Monster",
                    Extension = "jpg",
                },
                new LRFile {
                    Directory = "Sub/Directory/2020.002 Test 36. Blah Blubb/",
                    BaseName = "IMG_2853b",
                    Extension = "jpg",
                }
            };
            var expected = new[] {
                "2020.001.001.jpg",
                "2020.001.002.jpg",
                "2020.001.001.B1.jpg",
                "2020.002.003.B2.jpg"
            };
            var extractors = FieldExtractorFactory.I.FromString(@"{!directory!(\d{4}\.\d{3})[^/]*/$!\1}." 
                + @"{!basename!^(\S{3}_\d{4})([a-z])?!\sequence-distinct(\1)(1)(3)\replace-if(\2)(!a!b!c!d)(!.B1!.B2!.B3!.B4)}."
	            + @"{!ext!(.*)!\tolower(\1)}");
            Assert.AreEqual(5, extractors.Count);
            for (int i = 0; i < files.Length; i++) {
                var res = string.Join("", extractors.Select(x => x.Extract(files[i])));
                Assert.AreEqual(expected[i], res);
            }
        }

        [TestMethod]
        public void TestReplaceRegexText() {
            var list = FieldExtractorFactory.I.FromString(@"{!x!x!\text(blah\2)}");
            var cmdList = (list.Single() as RegexFieldExtractor).ReplaceCommandList;
            Assert.AreEqual(1, cmdList.Count);
            Assert.AreEqual("text", cmdList[0].Name);
            var res = cmdList.Run(new[] { "IMG_1234a", "IMG_1234", "a" });
            Assert.AreEqual("blaha", res);
        }

        [TestMethod]
        public void TestReplaceRegexTextGroup() {
            var list = FieldExtractorFactory.I.FromString(@"{!filename!^(\S{3}_\d{4})([a-z])!\1}");
            var cmdList = (list.Single() as RegexFieldExtractor).ReplaceCommandList;
            Assert.AreEqual(1, cmdList.Count);
            Assert.AreEqual("text", cmdList[0].Name);
            var res = cmdList.Run(new[] { "IMG_1234a",  "IMG_1234", "a" });
            Assert.AreEqual("IMG_1234", res);
        }

        [TestMethod]
        public void TestReplaceRegexSequence() {
            var list = FieldExtractorFactory.I.FromString(@"{!x!x!\sequence(2)(3)}");
            var cmdList = (list.Single() as RegexFieldExtractor).ReplaceCommandList;
            Assert.AreEqual(1, cmdList.Count);
            Assert.AreEqual("sequence", cmdList[0].Name);
            string res;
            res = cmdList.Run(new[] { "a" }); Assert.AreEqual("002", res);
            res = cmdList.Run(new[] { "b" }); Assert.AreEqual("003", res);
            res = cmdList.Run(new[] { "a" }); Assert.AreEqual("004", res);
        }

        [TestMethod]
        public void TestReplaceRegexSequenceDistinct() {
            var list = FieldExtractorFactory.I.FromString(@"{!x!x!\sequence-distinct(\0)(2)(3)}");
            var cmdList = (list.Single() as RegexFieldExtractor).ReplaceCommandList;
            Assert.AreEqual(1, cmdList.Count);
            Assert.AreEqual("sequence-distinct", cmdList[0].Name);
            string res;
            res = cmdList.Run(new[] { "a" }); Assert.AreEqual("002", res);
            res = cmdList.Run(new[] { "b" }); Assert.AreEqual("003", res);
            res = cmdList.Run(new[] { "a" }); Assert.AreEqual("002", res);
        }

        [TestMethod]
        public void TestReplaceRegexSequenceDistinctDifferentBases() {
            var list = FieldExtractorFactory.I.FromString(@"{|basename|(\d{4}\.\d{3}\.\d{4})([a-z])?|\sequence-distinct(\2)(1)(3)(\1)}");
            var cmdList = (list.Single() as RegexFieldExtractor).ReplaceCommandList;
            Assert.AreEqual(1, cmdList.Count);
            Assert.AreEqual("sequence-distinct", cmdList[0].Name);
            string res;
            res = cmdList.Run(new[] { "2020.001.1234", "2020.001", "1234" }); Assert.AreEqual("001", res);
            res = cmdList.Run(new[] { "2020.002.2234a", "2020.002", "2234" }); Assert.AreEqual("001", res);
            res = cmdList.Run(new[] { "2020.002.2235", "2020.002", "2235" }); Assert.AreEqual("002", res);
            res = cmdList.Run(new[] { "2020.002.2234", "2020.002", "2234" }); Assert.AreEqual("001", res);
            res = cmdList.Run(new[] { "2020.001.1234a", "2020.001", "1234" }); Assert.AreEqual("001", res);
            res = cmdList.Run(new[] { "2020.001.1235", "2020.001", "1235" }); Assert.AreEqual("002", res);
        }

        [TestMethod]
        public void TestReplaceRegexReplace() {
            var list = FieldExtractorFactory.I.FromString(@"{!x!x!\replace(\0)(b)(+\1+)}");
            var cmdList = (list.Single() as RegexFieldExtractor).ReplaceCommandList;
            Assert.AreEqual(1, cmdList.Count);
            Assert.AreEqual("replace", cmdList[0].Name);
            string res;
            res = cmdList.Run(new[] { "abc", "X" }); Assert.AreEqual("a+X+c", res);
        }

        [TestMethod]
        public void TestReplaceRegexReplaceIf() {
            var list = FieldExtractorFactory.I.FromString(@"{!x!x!\replace-if(\0)(!a!b!c!d)(!.B1\1!.B2!.B3!.B4)}");
            var cmdList = (list.Single() as RegexFieldExtractor).ReplaceCommandList;
            Assert.AreEqual(1, cmdList.Count);
            Assert.AreEqual("replace-if", cmdList[0].Name);
            string res;
            res = cmdList.Run(new[] { "aa", "X" }); Assert.AreEqual(".B1X.B1X", res);
            res = cmdList.Run(new[] { "b", "X" }); Assert.AreEqual(".B2", res);
            res = cmdList.Run(new[] { "c", "X" }); Assert.AreEqual(".B3", res);
            res = cmdList.Run(new[] { "d", "X" }); Assert.AreEqual(".B4", res);
            res = cmdList.Run(new[] { "e", "X" }); Assert.AreEqual("e", res);
        }

        [TestMethod]
        public void TestReplaceRegexLowerUpper() {
            var list = FieldExtractorFactory.I.FromString(@"{!x!x!\tolower(\0)\toupper(\0)}");
            var cmdList = (list.Single() as RegexFieldExtractor).ReplaceCommandList;
            Assert.AreEqual(2, cmdList.Count);
            Assert.AreEqual("tolower", cmdList[0].Name);
            Assert.AreEqual("toupper", cmdList[1].Name);
            string res;
            res = cmdList.Run(new[] { "aBc" }); Assert.AreEqual("abcABC", res);
        }
    }
}
